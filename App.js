import React from 'react';
import {store} from './src/redux/store';
import {Provider} from 'react-redux';
import RootNavigator from './src/navigations/navigators/rootNavigator';
import {Root} from 'native-base';

const App = () => {
  return (
    <Provider store={store}>
      <Root>
        <RootNavigator />
      </Root>
    </Provider>
  );
};

export default App;
