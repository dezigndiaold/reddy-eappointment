Saugata, 11:18 PM
Appointment Details By Date and time
Url : https://e-appointment.fbmlive.com/api/appointment_Details
Headers:
{
 'Content-Type': 'application/json
}
Parameters:
{
doctor_id,
}
Output:{
 "data": [
 {
 "id": 9,
"doctor_id": "5",
 "app_date": "2020-05-14",
 "app_time": "13:00",
 "status": "Open"
 }
 ],
 "error": null
}

Saugata, 11:19 PM
Morning Appointments
Url : https://e-appointment.fbmlive.com/api/docmrng_appointment
Headers:
{
 'Content-Type': 'application/json
}
Parameters:
{
doctor_id,
}
Output:{
"mrngArray": [
 "12:00",
 "12:05",
 "12:10",
 "12:15",
 "12:20",
 "12:25"],
 "error": null
}

Saugata, 11:19 PM
Evening Appointments
Url : https://e-appointment.fbmlive.com/api/docevn_appointment
Headers:
{
 'Content-Type': 'application/json
}
Parameters:
{
doctor_id,
}
Output:{
"EveningArray": [
 "18:00",
 "18:05",
 "18:10",
 "18:15",
 "18:20",
 "18:25",
 "18:30",
 "18:35",
 ],
 "error": null
}