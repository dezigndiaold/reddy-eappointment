import {StyleSheet} from 'react-native';
import {themeVars} from './themeVars';

export const drawerStyles = StyleSheet.create({
  drawerBgClr: {
    backgroundColor: themeVars.themeColor,
  },
  label: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
  },
  drawerBodyItem: {
    paddingVertical: 5,
  },
  icon: {
    color: 'white',
  },
  drawerBody: {
    width: 230,
    backgroundColor: themeVars.themeColor,
  },
  drawerAnimation: {
    flex: 1,
    shadowColor: '#FFF',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 5,
  },
});
