import {StyleSheet} from 'react-native';
import {themeVars} from './themeVars';

export const splashStyles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 40,
    fontWeight: 'bold',
    color: themeVars.themeColor,
  },
  icon: {
    width: 50,
    height: 50,
  },
});
