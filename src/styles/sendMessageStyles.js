import {StyleSheet} from 'react-native';
import {themeVars} from './themeVars';

export const sendMessageStyles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    elevation: 8,
    backgroundColor: 'white',
  },
  tableFixedHeader: {
    backgroundColor: 'white',
    elevation: 10,
  },
  containerTitle: {
    color: themeVars.themeColor,
    fontSize: 22,
    alignSelf: 'center',
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 20,
  },
  tableHeader: {
    marginTop: 20,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
  },
  tableHeaderTxt: {
    color: themeVars.themeColor,
    fontSize: 16,
    alignSelf: 'center',
    fontWeight: 'bold',
    marginBottom: 10,
  },
  columns: {
    minHeight: 60,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'lightgray',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  countColumns: {
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'lightgray',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  selected: {
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'lightgrey',
  },
  columnTitle: {
    color: themeVars.themeColor,
    fontWeight: 'bold',
    fontSize: 16,
    paddingVertical: 10,
    alignSelf: 'center',
  },
  btn: {
    marginVertical: 20,
    width: 120,
    alignSelf: 'center',
    borderRadius: 50,
  },
  btnBgClr: {
    backgroundColor: themeVars.themeColor,
  },
  errorTxt: {
    marginTop: 10,
    alignSelf: 'center',
    color: 'red',
    fontWeight: 'bold',
  },
});
