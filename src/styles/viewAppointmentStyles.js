import {StyleSheet} from 'react-native';
import {themeVars} from './themeVars';

export const viewAppointmentStyles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    elevation: 8,
    backgroundColor: 'white',
  },

  containerTitle: {
    color: themeVars.themeColor,
    fontSize: 22,
    alignSelf: 'center',
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 20,
  },
  tableHeader: {
    marginTop: 20,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
  },
  tableHeaderTxt: {
    color: themeVars.themeColor,
    fontSize: 16,
    alignSelf: 'center',
    fontWeight: 'bold',
    marginBottom: 10,
  },
  filterContainer: {
    paddingHorizontal: 20,
    marginTop: 10,
    flexDirection: 'row',
    marginBottom: 10,
  },
  searchFilterBtn: {
    flexDirection: 'row',
    borderRadius: 40,
    backgroundColor: 'white',
    elevation: 10,
    marginRight: 10,
    paddingHorizontal: 10,
    paddingTop: 2,
    borderWidth: 1,
    borderColor: 'lightgray',
  },
  searchFilterIcon: {
    color: 'gray',
    marginRight: 5,
    alignSelf: 'center',
  },
  searchFilterBtnTxt: {
    color: themeVars.themeColor,
    alignSelf: 'center',
    marginBottom: 5,
    fontWeight: 'bold',
  },
  dateFilterBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dateFilterIcon: {
    color: themeVars.themeColor,
    marginRight: 5,
  },
  dateFilterBtnTxt: {
    color: themeVars.themeColor,
    fontWeight: 'bold',
  },
  statusFilterBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
  },
  statusFilterIcon: {
    color: themeVars.themeColor,
    fontSize: 20,
    marginRight: 5,
    fontWeight: 'bold',
  },
  statusFilterBtnTxt: {
    backgroundColor: 'white',
    padding: 5,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'lightgray',
    color: themeVars.themeColor,
    fontWeight: 'bold',
    elevation: 10,
  },
  columnsHeader: {
    marginTop: 10,
    paddingHorizontal: 20,
    borderRadius: 10,
    marginHorizontal: 5,
    backgroundColor: themeVars.themeColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  columnHeaderTitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,

    paddingVertical: 10,
  },
  columns: {
    marginTop: 10,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'lightgray',

    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    flex: 1,
  },
  columnTitle: {
    color: themeVars.themeColor,
    fontWeight: 'bold',
    fontSize: 15,
    paddingVertical: 10,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'stretch',
  },

  columnIcon: {
    color: themeVars.themeColor,
    fontWeight: 'bold',
    flex: 0.5,
    alignSelf: 'stretch',

    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  btn: {
    marginVertical: 20,
    width: 120,
    alignSelf: 'center',
    borderRadius: 50,
  },
  btnBgClr: {
    backgroundColor: themeVars.themeColor,
  },
  dateFilterSection: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 10,
    borderColor: 'lightgrey',
    borderWidth: 1,
    marginHorizontal: 20,
    paddingRight: 5,
  },
  statusFilterSection: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 10,
    marginHorizontal: 20,
  },
  statusFilterItem: {
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 10,
  },
  statusFilterItemTxt: {
    fontSize: 18,
    paddingRight: 10,
  },
  dateInputSection: {
    flexDirection: 'row',
    flexGrow: 1,
  },
  dayInput: {
    width: 40,
    height: '100%',
    paddingHorizontal: 8,
    color: 'lightgrey',
  },
  dayInput: {
    width: 40,
    height: '100%',
    paddingHorizontal: 8,
    color: 'gray',
  },
  monthInput: {
    marginLeft: 5,
    width: 50,
    height: '100%',
    paddingHorizontal: 8,
    color: 'gray',
  },
  yearInput: {
    marginLeft: 5,
    height: '100%',
    paddingHorizontal: 8,
    color: 'gray',
  },
  dateHyphon: {
    color: 'grey',
    fontSize: 18,
    alignSelf: 'center',
  },
  dateSearchBtn: {
    backgroundColor: themeVars.themeColor,
  },
  customRadio: {
    width: 20,
    height: 20,

    borderWidth: 2,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  radioFill: {
    width: 10,
    height: 10,
    backgroundColor: themeVars.themeColor,
    borderRadius: 50,
  },
});
