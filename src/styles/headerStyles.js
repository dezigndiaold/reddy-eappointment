import {StyleSheet} from 'react-native';
import {themeVars} from './themeVars';

export const headerStyles = StyleSheet.create({
  header: {
    height: 75,
    backgroundColor: themeVars.themeColor,
    borderBottomEndRadius: 40,
    borderBottomStartRadius: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40,
  },
  icon: {
    color: 'white',
    fontSize: 40,
  },
  logo: {
    height: 36,
    width: 36,
  },
  ml20: {
    marginLeft: 10,
  },
  title: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  drawerIcon: {
    position: 'absolute',
    top: 0,
    left: 0,
    margin: 10,
  },
});
