import {StyleSheet} from 'react-native';
import {themeVars} from './themeVars';

export const createAppointmentStyles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingHorizontal: 20,
  },
  itemLabel: {
    fontWeight: 'bold',
    marginTop: 5,
    color: themeVars.themeColor,
    marginLeft: 10,
  },
  containerTitle: {
    marginTop: 10,
    marginBottom: 20,
    color: themeVars.themeColor,
    fontSize: 22,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  inputItem: {
    borderWidth: 2,
    borderColor: 'lightgray',
    marginVertical: 10,
  },
  dateInputItem: {
    borderWidth: 2,
    borderColor: 'lightgray',
    marginVertical: 10,

    height: 50,
  },
  commentInputItem: {
    marginVertical: 10,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: 'lightgray',
  },
  input: {
    paddingLeft: 10,
    color: 'gray',
  },
  dateInput: {
    paddingLeft: 10,
    color: 'gray',
    alignSelf: 'center',
    fontSize: 17,
  },
  commentInput: {
    paddingTop: 10,
    textAlign: 'left',
    color: 'gray',
    textAlignVertical: 'top',
  },
  submitBtn: {
    marginVertical: 10,
    width: 150,
    alignSelf: 'center',
    borderRadius: 50,
  },
  savedContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  btn: {
    marginTop: 20,
    width: 120,
    alignSelf: 'center',
    borderRadius: 50,
    marginLeft: 10,
    marginBottom: 20,
  },
  btnBgClr: {
    backgroundColor: themeVars.themeColor,
  },
  savedBtnBgClr: {
    backgroundColor: 'lightgreen',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  savedTxt: {
    color: 'white',
    fontSize: 20,
    marginHorizontal: 10,
    paddingVertical: 10,
  },
  savedIcon: {
    color: 'white',
  },
  errorIcon: {
    color: 'red',
  },
  errorTxt: {
    alignSelf: 'center',
    color: 'red',
    fontWeight: 'bold',
  },
  statusContainer: {
    marginTop: 10,
    marginBottom: 20,
  },
  currentStatusContainer: {
    flexDirection: 'row',
    padding: 10,
  },
  currentStatusLabel: {
    color: themeVars.themeColor,
    fontSize: 20,
    fontWeight: 'bold',
  },
  currentStatusBtnLabel: {
    color: themeVars.themeColor,
    fontSize: 14,
    marginLeft: 5,
    paddingTop: 5,
    fontWeight: 'bold',
  },
  currentStatusBtnContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  currentStatusBtn: {
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  cancelIcon: {
    color: 'orange',
  },
  editIconContainer: {
    marginTop: 30,
    alignItems: 'flex-end',
  },
  editIcon: {
    color: themeVars.themeColor,
    fontWeight: 'bold',
  },
  editBtn: {
    flexDirection: 'row',
  },
  editIconTxt: {
    color: themeVars.themeColor,
    fontWeight: 'bold',
    paddingRight: 10,
    paddingLeft: 5,
    fontSize: 18,
  },
  footerImage: {
    height: 120,
    alignSelf: 'center',
    width: 300,

    marginBottom: 20,
  },
  timeIcon: {
    marginVertical: 15,
    marginHorizontal: 20,
    alignItems: 'center',
  },
  timeInputfield: {
    flexGrow: 1,
    borderWidth: 2,
    borderColor: 'lightgray',
    marginVertical: 10,
    height: 50,
    marginRight: 10,
  },
  timeIconText: {
    color: 'grey',
    marginTop: 10,
  },
  availableTimeHeading: {
    alignSelf: 'center',
    fontSize: 18,
    marginVertical: 10,
    textAlign: 'center',
    color: 'grey',
  },
  pickerContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: 'lightgray',
    alignItems: 'center',
    paddingRight: 5,
    borderRadius: 4,
  },
  picker: {
    width: 120,
  },
  timeLoader: {
    height: 100,
  },
  timeLoaderText: {
    color: themeVars.themeColor,
    fontSize: 18,
    textAlign: 'center',
  },
  availableTimeError: {
    margin: 20,
    alignItems: 'center',
  },
  availableTimeErrorIcon: {
    color: 'salmon',
    fontSize: 70,
  },
  availableTimeErrorrText: {
    color: 'salmon',
    margin: 10,
    fontSize: 18,
    textAlign: 'center',
  },
  timeCanacelBtnClr: {
    backgroundColor: themeVars.themeColor,
  },
  timeCanacelBtn: {
    paddingRight: 5,
  },
  modalWrapper: {
    flex: 1,
    maxHeight: 300,
    paddingVertical: 10,
    width: '100%',
    backgroundColor: '#fff',
    borderRadius: 30,
    position: 'absolute',
    top: 151,
    left: 20,
    zIndex: 1,
    elevation: 1,
  },
  phoneNumber: {
    marginLeft: 20,
    marginTop: 20,
    fontSize: 16,
  },
});
