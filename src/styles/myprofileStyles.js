import {StyleSheet} from 'react-native';
import {themeVars} from './themeVars';

export const myProfileStyles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingHorizontal: 20,
    marginTop: 10,
  },
  containerTitle: {
    color: themeVars.themeColor,
    fontSize: 22,
    alignSelf: 'center',
    fontWeight: 'bold',
    marginBottom: 10,
  },
  editIconContainer: {
    marginTop: 30,
    alignItems: 'flex-end',
  },
  editIcon: {
    color: themeVars.themeColor,
    fontWeight: 'bold',
  },
  editBtn: {
    flexDirection: 'row',
  },
  editIconTxt: {
    color: themeVars.themeColor,
    fontWeight: 'bold',
    paddingRight: 10,
    paddingLeft: 5,
    fontSize: 18,
  },
  formContainer: {
    marginTop: 10,
    marginBottom: 10,
    padding: 20,
    borderRadius: 20,
    backgroundColor: 'white',
    elevation: 10,
  },
  inputItem: {
    borderWidth: 2,
    borderColor: 'gray',
    marginVertical: 15,
  },
  input: {
    paddingLeft: 10,
    color: 'gray',
  },
  btn: {
    marginVertical: 10,
    width: 120,
    alignSelf: 'center',
    borderRadius: 50,
  },
  btnBgClr: {
    backgroundColor: themeVars.themeColor,
  },
  savedBtnBgClr: {
    backgroundColor: 'lightgreen',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  savedTxt: {
    color: 'white',
    fontSize: 20,
    marginHorizontal: 10,
    paddingVertical: 10,
  },
  savedIcon: {
    color: 'white',
  },
  errorIcon: {
    color: 'red',
  },
  errorTxt: {
    alignSelf: 'center',
    color: 'red',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  timingContianer: {
    marginTop: 20,
    paddingHorizontal: 20,
  },
  timingHeading: {
    alignSelf: 'center',
    color: 'gray',
    fontSize: 20,
  },
  timingItem: {
    marginTop: 15,
    flexDirection: 'row',
  },
  timingLabel: {
    color: 'gray',
    fontWeight: 'bold',
    fontSize: 20,
  },
  timingInput: {
    color: 'gray',
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
    paddingBottom: 0,
    bottom: 25,
    marginHorizontal: 10,
    textAlign: 'center',
  },
});
