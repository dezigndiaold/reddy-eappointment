import React from 'react';
import {Text, StatusBar, TouchableOpacity} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {headerStyles} from '../../styles/headerStyles';
import {Icon, Thumbnail, View} from 'native-base';
import logo from '../../assets/images/calendar-and-clock.png';

const HomeHeader = props => {
  return (
    <>
      <StatusBar backgroundColor="white" barStyle="dark-content" />

      <Animatable.View animation="slideInDown" style={headerStyles.header}>
        <View style={headerStyles.drawerIcon}>
          <TouchableOpacity onPress={props.onIconPress}>
            {!props.drawerItem && (
              <Icon
                name="menu"
                type="MaterialCommunityIcons"
                style={headerStyles.icon}
              />
            )}
            {props.drawerItem && (
              <Icon
                name="arrow-left"
                type="MaterialCommunityIcons"
                style={headerStyles.icon}
              />
            )}
          </TouchableOpacity>
        </View>
        <Thumbnail
          square
          source={logo}
          style={[headerStyles.logo, headerStyles.ml20]}
        />

        <Text style={headerStyles.title}> eAppointment</Text>
      </Animatable.View>
    </>
  );
};

export default HomeHeader;
