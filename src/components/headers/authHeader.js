import React from 'react';
import {Text, StatusBar} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {headerStyles} from '../../styles/headerStyles';
import {Icon, Thumbnail} from 'native-base';
import logo from '../../assets/images/calendar-and-clock.png';
const AuthHeader = () => {
  return (
    <>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <Animatable.View animation="slideInDown" style={headerStyles.header}>
        <Thumbnail square source={logo} style={headerStyles.logo} />
        <Text style={headerStyles.title}> eAppointment</Text>
      </Animatable.View>
    </>
  );
};

export default AuthHeader;
