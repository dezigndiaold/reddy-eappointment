import React from 'react';
import {connect} from 'react-redux';
import {DrawerItem, DrawerContentScrollView} from '@react-navigation/drawer';
import {Text, Alert} from 'react-native';
import {Icon} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import {setToken} from '../../redux/actions/utilsActions';
import {drawerStyles} from '../../styles/drawerStyles';

const CustomDrawer = props => {
  async function handleLogOut() {
    await props.navigation.toggleDrawer();
    Alert.alert('Hold on!', 'Are you sure you want to Logout?', [
      {
        text: 'Logout',
        onPress: () => {
          AsyncStorage.removeItem('token');
          AsyncStorage.removeItem('userData');
          AsyncStorage.removeItem('allAppointments');

          props.setToken(null);
          props.navigation.navigate('auth', {
            screen: 'auth-login',
          });
        },
      },
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
    ]);
  }

  return (
    <DrawerContentScrollView {...props}>
      <DrawerItem
        label={() => <Text style={drawerStyles.label}>My profile</Text>}
        style={drawerStyles.drawerBodyItem}
        icon={() => (
          <Icon name="person" type="MaterialIcons" style={drawerStyles.icon} />
        )}
        onPress={() => {
          props.navigation.toggleDrawer();
          props.navigation.navigate('myprofile');
        }}
      />
      <DrawerItem
        label={() => <Text style={drawerStyles.label}>Create Appointment</Text>}
        style={drawerStyles.drawerBodyItem}
        icon={() => (
          <Icon
            name="folder-plus"
            type="FontAwesome5"
            style={drawerStyles.icon}
          />
        )}
        onPress={() => {
          props.navigation.toggleDrawer();
          props.navigation.navigate('createappointment');
        }}
      />
      <DrawerItem
        label={() => <Text style={drawerStyles.label}>View Appointment</Text>}
        style={drawerStyles.drawerBodyItem}
        icon={() => (
          <Icon
            name="feature-search"
            type="MaterialCommunityIcons"
            style={drawerStyles.icon}
          />
        )}
        onPress={() => {
          props.navigation.toggleDrawer();
          props.navigation.navigate('viewappointment');
        }}
      />
      <DrawerItem
        label={() => <Text style={drawerStyles.label}>Send Message</Text>}
        style={drawerStyles.drawerBodyItem}
        icon={() => (
          <Icon
            name="message-outline"
            type="MaterialCommunityIcons"
            style={drawerStyles.icon}
          />
        )}
        onPress={() => {
          props.navigation.toggleDrawer();
          props.navigation.navigate('sendmessage');
        }}
      />
      <DrawerItem
        label={() => <Text style={drawerStyles.label}>Logout</Text>}
        style={drawerStyles.drawerBodyItem}
        icon={() => (
          <Icon
            name="logout"
            type="SimpleLineIcons"
            style={drawerStyles.icon}
          />
        )}
        onPress={() => {
          props.navigation.toggleDrawer();
          handleLogOut();
        }}
      />
    </DrawerContentScrollView>
  );
};
const mapStateToProps = state => ({
  utils: state.utils,
});
const mapDispatchToProps = dispatch => ({
  setToken: token => dispatch(setToken(token)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomDrawer);
