import React from 'react';
import {
  Text,
  SafeAreaView,
  TouchableOpacity,
  BackHandler,
  View,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {myProfileStyles} from '../../styles/myprofileStyles';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {Input, Item, Icon} from 'native-base';
import {Button} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import HomeHeader from '../headers/homeHeader';
import {BarIndicator} from 'react-native-indicators';
import AsyncStorage from '@react-native-community/async-storage';
import LoadingComponent from '../loadingComponent';
import {editUserDetails} from '../../apis/user-info';

const MyProfile = props => {
  const [show, setShow] = React.useState(false);
  const [datePickerValue, setDatePickerValue] = React.useState(new Date());
  const [typeoftiming, setTypeoftiming] = React.useState('');
  const [isTimeChange, setTimeChange] = React.useState(true);
  const [isLoading, setLoading] = React.useState(false);
  const [userId, setUserId] = React.useState('');
  const [name, setName] = React.useState('');
  const [clinic, setCilinic] = React.useState('');
  const [phone, setPhone] = React.useState('');
  const [morningTimeStart, setMorningStart] = React.useState('');
  const [morningTimeEnd, setMorningEnd] = React.useState('');
  const [eveningTimeStart, setEveningStart] = React.useState('');
  const [eveningTimeEnd, setEveningEnd] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [confirmPassword, setConfirmPassword] = React.useState('');

  const [canEdit, toggleCanEdit] = React.useState(false);
  const [error, setError] = React.useState('');
  const [errorType, setErrorType] = React.useState('');
  const [processingState, setProcessingState] = React.useState('');

  function onSubmit() {
    setErrorType('');
    setError('');
    if (!name) {
      setError("Doctor's name can't be empty!");
      setErrorType('name');
    } else if (!clinic) {
      setError("Clinic's name can't be empty!");
      setErrorType('clicnic');
    } else if (!phone || phone.length !== 10) {
      setError('Phone number must be 10 digits!');
      setErrorType('phone');
    } else if (!password) {
      setError('Enter your last password or a new password!');
      setErrorType('password');
    } else if (password.length < 8) {
      setError('Password atleast be 8 characters!');
      setErrorType('password');
    } else if (!confirmPassword || password !== confirmPassword) {
      setError('Password not matched!');
      setErrorType('confirmpassword');
    } else {
      setProcessingState('loading');
      console.log(
        userId,
        name,
        clinic,
        phone,
        password,
        confirmPassword,
        morningTimeStart,
        morningTimeEnd,
        eveningTimeStart,
        eveningTimeEnd,
      );
      editUserDetails(
        userId,
        name,
        clinic,
        phone,
        password,
        confirmPassword,
        morningTimeStart,
        morningTimeEnd,
        eveningTimeStart,
        eveningTimeEnd,
      )
        .then(async res => {
          const userData = {
            id: userId,
            doctor_name: name,
            doctor_clinic: clinic,
            doctor_number: phone,
            original_password: password,
            morning_to: morningTimeStart,
            morning_from: morningTimeEnd,
            evening_to: eveningTimeStart,
            evening_from: eveningTimeEnd,
          };

          await AsyncStorage.setItem('userData', JSON.stringify(userData));

          setError('');
          setErrorType('');
          setConfirmPassword('');
          setPassword('');
          toggleCanEdit(false);
          setProcessingState('saved');
        })
        .catch(err => {
          setError('Error occured!');
          setErrorType('server!');
          setProcessingState('');
        });
    }
  }

  function handleEdit() {
    if (processingState !== '') setProcessingState('');
    toggleCanEdit(!canEdit);
    setError('');
    setErrorType('');
    setPassword('');
    setConfirmPassword('');
    if (processingState !== 'saved') {
      getUserData();
    }
  }

  React.useEffect(() => {
    setLoading(true);
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    getUserData();
    return () => backHandler.remove();
  }, []);

  async function getUserData() {
    let data = await AsyncStorage.getItem('userData');
    data = JSON.parse(data);
    setUserId(data.id);
    setName(data.doctor_name);
    setCilinic(data.doctor_clinic);
    setPhone(data.doctor_number);
    setMorningStart(data.morning_to);
    setMorningEnd(data.morning_from);
    setEveningStart(data.evening_to);
    setEveningEnd(data.evening_from);
    setLoading(false);
  }
  const backAction = () => {
    props.navigation.goBack();
    return true;
  };
  const onTimeChange = selectedDate => {
    const currentDate = selectedDate;
    setShow(Platform.OS === 'ios');
    if (selectedDate) {
      setDatePickerValue(currentDate);
      let hour = currentDate.getHours();
      let minutes = currentDate.getMinutes();
      if (hour === 0) {
        hour = '00';
      }
      if (minutes === 0) {
        minutes = '00';
      }
      hour = '0' + hour;
      minutes = '0' + minutes;
      const finalTime = hour.slice(-2) + ':' + minutes.slice(-2);

      if (typeoftiming === 'morning-start') {
        setMorningStart(finalTime);
      } else if (typeoftiming === 'morning-end') {
        setMorningEnd(finalTime);
      } else if (typeoftiming === 'evening-start') {
        setEveningStart(finalTime);
      } else if (typeoftiming === 'evening-end') {
        setEveningEnd(finalTime);
      }
    }
    setShow(false);
    setError('');
    setErrorType('');
  };
  function onCancelDate() {
    setDatePickerValue(new Date());
    setMorningStart(morningTimeStart);
    setMorningEnd(morningTimeEnd);
    setEveningStart(eveningTimeStart);
    setEveningEnd(eveningTimeEnd);

    setShow(false);
    setError('');
    setErrorType('');
  }
  return (
    <>
      {!isLoading && (
        <SafeAreaView>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
            <HomeHeader
              drawerItem={true}
              onIconPress={() => {
                props.navigation.goBack();
              }}
            />
            <Animatable.View
              animation="fadeIn"
              style={myProfileStyles.mainContainer}>
              <Text style={myProfileStyles.containerTitle}> My Profile</Text>
              {processingState !== 'loading' && (
                <View style={myProfileStyles.editIconContainer}>
                  <TouchableOpacity
                    style={myProfileStyles.editBtn}
                    onPress={() => handleEdit()}>
                    <Icon
                      name={
                        canEdit ? 'pencil-remove-outline' : 'pencil-outline'
                      }
                      type="MaterialCommunityIcons"
                      style={myProfileStyles.editIcon}
                    />
                    {processingState === '' && !canEdit && (
                      <Text style={myProfileStyles.editIconTxt}>Can Edit</Text>
                    )}
                    {processingState === 'saved' && !canEdit && (
                      <Text style={myProfileStyles.editIconTxt}>
                        Edit Again
                      </Text>
                    )}
                    {canEdit && (
                      <Text style={myProfileStyles.editIconTxt}>
                        Cancel Edit
                      </Text>
                    )}
                  </TouchableOpacity>
                </View>
              )}
              <View style={myProfileStyles.formContainer}>
                <Item rounded style={myProfileStyles.inputItem}>
                  <Input
                    disabled={canEdit === false}
                    style={myProfileStyles.input}
                    placeholderTextColor="gray"
                    placeholder="Doctor Name"
                    value={name}
                    onChangeText={text => {
                      setName(text);
                      setError('');
                      setErrorType('');
                    }}
                  />
                  {(errorType === 'name' || errorType === 'credential') &&
                    canEdit && (
                      <Icon
                        active
                        name="close-circle"
                        style={myProfileStyles.errorIcon}
                      />
                    )}
                </Item>
                <Item rounded style={myProfileStyles.inputItem}>
                  <Input
                    disabled={canEdit === false}
                    style={myProfileStyles.input}
                    placeholderTextColor="gray"
                    placeholder="Clinic Name"
                    value={clinic}
                    onChangeText={text => {
                      setCilinic(text);
                      setError('');
                      setErrorType('');
                    }}
                  />
                  {(errorType === 'clinic' || errorType === 'credential') &&
                    canEdit && (
                      <Icon
                        active
                        name="close-circle"
                        style={myProfileStyles.errorIcon}
                      />
                    )}
                </Item>
                <Item rounded style={myProfileStyles.inputItem}>
                  <Input
                    disabled={canEdit === false}
                    style={myProfileStyles.input}
                    placeholderTextColor="gray"
                    placeholder="Mobile Number"
                    keyboardType="phone-pad"
                    maxLength={10}
                    value={phone}
                    onChangeText={text => {
                      setPhone(text);
                      setError('');
                      setErrorType('');
                    }}
                  />

                  {(errorType === 'phone' || errorType === 'credential') &&
                    canEdit && (
                      <Icon
                        active
                        name="close-circle"
                        style={myProfileStyles.errorIcon}
                      />
                    )}
                </Item>
                <View style={myProfileStyles.timingContianer}>
                  <Text style={myProfileStyles.timingHeading}>Clinic Time</Text>

                  <View style={myProfileStyles.timingItem}>
                    <Text style={myProfileStyles.timingLabel}>Morning</Text>
                    <TouchableOpacity
                      onPress={() => {
                        if (canEdit) {
                          setTypeoftiming('morning-start');
                          setShow(true);
                          setError('');
                          setErrorType('');
                        }
                      }}>
                      <Input
                        disabled={true}
                        maxLength={5}
                        keyboardType="number-pad"
                        value={morningTimeStart}
                        style={myProfileStyles.timingInput}
                        onChangeText={text => {
                          if (
                            text === '' ||
                            text === '00' ||
                            (parseInt(text) >= 0 && parseInt(text) <= 12)
                          ) {
                            setMorningStart(text);
                          }
                        }}
                      />
                    </TouchableOpacity>
                    <Text style={myProfileStyles.timingLabel}>to</Text>
                    <TouchableOpacity
                      onPress={() => {
                        if (canEdit) {
                          setTypeoftiming('morning-end');
                          setShow(true);
                          setError('');
                          setErrorType('');
                        }
                      }}>
                      <Input
                        disabled={true}
                        maxLength={5}
                        keyboardType="number-pad"
                        value={morningTimeEnd}
                        style={myProfileStyles.timingInput}
                        onChangeText={text => {
                          setMorningEnd(text);
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={myProfileStyles.timingItem}>
                    <Text style={myProfileStyles.timingLabel}>Evening</Text>
                    <TouchableOpacity
                      onPress={() => {
                        if (canEdit) {
                          setTypeoftiming('evening-start');
                          setShow(true);
                          setError('');
                          setErrorType('');
                        }
                      }}>
                      <Input
                        disabled={true}
                        maxLength={5}
                        keyboardType="number-pad"
                        value={eveningTimeStart}
                        style={myProfileStyles.timingInput}
                        onChangeText={text => {
                          setEveningStart(text);
                        }}
                      />
                    </TouchableOpacity>

                    <Text style={myProfileStyles.timingLabel}>to</Text>
                    <TouchableOpacity
                      onPress={() => {
                        if (canEdit) {
                          setTypeoftiming('evening-end');
                          setShow(true);
                          setError('');
                          setErrorType('');
                        }
                      }}>
                      <Input
                        disabled={true}
                        maxLength={5}
                        keyboardType="number-pad"
                        value={eveningTimeEnd}
                        style={myProfileStyles.timingInput}
                        onChangeText={text => {
                          setEveningEnd(text);
                          setError('');
                          setErrorType('');
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                {canEdit && (
                  <>
                    <Item rounded style={myProfileStyles.inputItem}>
                      <Input
                        disabled={canEdit === false}
                        style={myProfileStyles.input}
                        placeholderTextColor="gray"
                        placeholder="Password"
                        secureTextEntry={true}
                        value={password}
                        onChangeText={text => {
                          setPassword(text);
                          setError('');
                          setErrorType('');
                        }}
                      />

                      {(errorType === 'password' ||
                        errorType === 'credential') &&
                        canEdit && (
                          <Icon
                            active
                            name="close-circle"
                            style={myProfileStyles.errorIcon}
                          />
                        )}
                    </Item>
                    <Item rounded style={myProfileStyles.inputItem}>
                      <Input
                        disabled={canEdit === false}
                        style={myProfileStyles.input}
                        placeholderTextColor="gray"
                        placeholder="Confirm Password"
                        secureTextEntry={true}
                        value={confirmPassword}
                        onChangeText={text => {
                          setConfirmPassword(text);
                          setError('');
                          setErrorType('');
                        }}
                      />

                      {(errorType === 'confirmpassword' ||
                        errorType === 'credential') &&
                        canEdit && (
                          <Icon
                            active
                            name="close-circle"
                            style={myProfileStyles.errorIcon}
                          />
                        )}
                    </Item>
                  </>
                )}
                {errorType !== '' && canEdit && (
                  <Animatable.Text
                    animation="wobble"
                    style={myProfileStyles.errorTxt}>
                    {error}
                  </Animatable.Text>
                )}
                {processingState === '' && canEdit && (
                  <Button
                    title="Submit"
                    raised
                    containerStyle={myProfileStyles.btn}
                    buttonStyle={myProfileStyles.btnBgClr}
                    onPress={() => onSubmit()}
                  />
                )}
                {processingState === 'loading' && (
                  <Animatable.View
                    animation="pulse"
                    style={[myProfileStyles.btn, myProfileStyles.btnBgClr]}>
                    <BarIndicator color="white" />
                  </Animatable.View>
                )}
                {processingState === 'saved' && (
                  <Animatable.View
                    animation="rubberBand"
                    style={[
                      myProfileStyles.btn,
                      myProfileStyles.savedBtnBgClr,
                    ]}>
                    <Text style={myProfileStyles.savedTxt}>Saved</Text>
                    <Icon
                      name="done-all"
                      type="MaterialIcons"
                      style={myProfileStyles.savedIcon}
                    />
                  </Animatable.View>
                )}
              </View>
            </Animatable.View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
      {isLoading && <LoadingComponent />}
      <DateTimePickerModal
        isVisible={show}
        minimumDate={new Date()}
        testID="dateTimePicker"
        timeZoneOffsetInMinutes={0}
        minuteInterval={5}
        value={datePickerValue}
        mode={'time'}
        is24Hour={true}
        display="default"
        onConfirm={date => {
          onTimeChange(date);
        }}
        onCancel={() => {
          setShow(false);
          onCancelDate();
        }}
      />
    </>
  );
};

export default MyProfile;
