import React from 'react';
import {View} from 'react-native';
import {SkypeIndicator} from 'react-native-indicators';
import {themeVars} from '../styles/themeVars';
const LoadingComponent = props => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <SkypeIndicator size={50} color={themeVars.themeColor} />
    </View>
  );
};

export default LoadingComponent;
