import React from 'react';
import {
  Text,
  SafeAreaView,
  ScrollView,
  BackHandler,
  View,
  FlatList,
  TouchableOpacity,
  Alert,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {sendMessageStyles} from '../../styles/sendMessageStyles';
import {Button} from 'react-native-elements';
import HomeHeader from '../headers/homeHeader';
import {connect} from 'react-redux';
import LoadingComponent from '../loadingComponent';
import AsyncStorage from '@react-native-community/async-storage';
import {getAppointments} from '../../apis/user-info';
import {setAllAppointments} from '../../redux/actions/utilsActions';
import SendSMS from 'react-native-sms';
import moment from 'moment';

const SendMessage = props => {
  const [patientsData, setPateintsData] = React.useState([]);

  const [selectAll, setSelectAll] = React.useState(false);
  const [selectedPatients, setSelectedPatients] = React.useState([]);
  const [isLoading, setLoading] = React.useState(true);
  const [stickyHeader, setStickyHeader] = React.useState(false);
  const [isSelectionMode, setSelectionMode] = React.useState(false);
  const [tempSelection, setTemp] = React.useState(false);
  const [error, setError] = React.useState('');

  React.useEffect(() => {
    setLoading(true);
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    getData();

    return () => backHandler.remove();
  }, []);
  React.useEffect(() => {
    const updateList = patientsData.filter(obj => obj.isSelect === true);
    if (updateList.length !== patientsData.length) {
      setSelectAll(false);
    }
    if (updateList.length === patientsData.length) {
      setSelectAll(true);
    }
    setSelectedPatients(updateList);
  }, [tempSelection]);

  async function getData() {
    let doctor_data = await AsyncStorage.getItem('userData');
    doctor_data = JSON.parse(doctor_data);
    let localAppointments = await AsyncStorage.getItem('allAppointments');
    if (localAppointments) {
      props.setAllAppointments(JSON.parse(localAppointments));
      const allAppointments = JSON.parse(localAppointments);
      const todayDate = moment().format('YYYY-MM-DD');
      let todayAppointments = allAppointments.filter(
        item => item.app_date === todayDate,
      );
      todayAppointments = todayAppointments.map(item => ({
        ...item,
        isSelect: false,
        selectedClass: sendMessageStyles.columns,
      }));
      setPateintsData(todayAppointments);
      setLoading(false);
    }
    getAppointments(doctor_data.id)
      .then(res => {
        props.setAllAppointments(res.data);
        const allAppointments = res.data;
        const todayDate = moment().format('YYYY-MM-DD');
        let todayAppointments = allAppointments.filter(
          item => item.app_date === todayDate,
        );
        todayAppointments = todayAppointments.map(item => ({
          ...item,
          isSelect: false,
          selectedClass: sendMessageStyles.columns,
        }));

        setPateintsData(todayAppointments);

        AsyncStorage.removeItem('allAppointments');
        AsyncStorage.setItem('allAppointments', JSON.stringify(res.data));
        setLoading(false);
      })
      .catch(async err => {
        let localAppointments = await AsyncStorage.getItem('allAppointments');
        if (localAppointments) {
          props.setAllAppointments(JSON.parse(localAppointments));
          const allAppointments = JSON.parse(localAppointments);
          const todayDate = moment().format('YYYY-MM-DD');

          let todayAppointments = allAppointments.filter(
            item => item.app_date === todayDate,
          );
          todayAppointments = todayAppointments.map(item => ({
            ...item,
            isSelect: false,
            selectedClass: sendMessageStyles.columns,
          }));

          setPateintsData(todayAppointments);
        }
        setLoading(false);
        console.log(err);
      });
  }
  const backAction = () => {
    props.navigation.goBack();
    return true;
  };
  function onSelectAll(action) {
    setError('');
    if (action === 'select') {
      let allAppointments = patientsData.map(item => ({
        ...item,
        isSelect: true,
        selectedClass: sendMessageStyles.selected,
      }));
      setSelectedPatients(allAppointments);
      setPateintsData(allAppointments);
    } else {
      let allAppointments = patientsData.map(item => ({
        ...item,
        isSelect: false,
        selectedClass: sendMessageStyles.columns,
      }));
      setPateintsData(allAppointments);

      setSelectedPatients([]);
    }
  }
  function selectItem(item) {
    setTemp(!tempSelection);
    setSelectionMode(true);
    item.isSelect = !item.isSelect;
    item.selectedClass = item.isSelect
      ? sendMessageStyles.selected
      : sendMessageStyles.columns;
    const index = patientsData.findIndex(obj => obj.id === item.id);
    let data = patientsData;

    data[index] = item;

    setPateintsData(data);
    setError('');
  }
  function sendMessage() {
    Alert.alert(
      'Send Message',
      `You can send message to ${
        selectedPatients.length === patientsData.length
          ? 'all'
          : selectedPatients.length
      } users`,
      [
        {
          text: 'Send',
          onPress: () => {
            triggerMessage();
          },
        },
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
      ],
    );
  }
  function triggerMessage() {
    SendSMS.send(
      {
        body: '',
        recipients: selectedPatients.map(obj => obj.patient_number),
        successTypes: ['sent', 'queued'],
        allowAndroidSendWithoutReadPermission: true,
      },
      (completed, cancelled, error) => {
        let allAppointments = patientsData.map(item => ({
          ...item,
          isSelect: false,
          selectedClass: sendMessageStyles.columns,
        }));
        setPateintsData(allAppointments);
        setSelectedPatients([]);
        setSelectionMode(false);
        setSelectAll(false);
        console.log(
          'SMS Callback: completed: ' +
            completed +
            ' cancelled: ' +
            cancelled +
            'error: ' +
            error,
        );
      },
    );
  }

  return (
    <>
      {!isLoading && (
        <SafeAreaView>
          <ScrollView
            showsVerticalScrollIndicator={false}
            stickyHeaderIndices={[2]}
            onScroll={e => {
              if (e.nativeEvent.contentOffset.y > 180) {
                setStickyHeader(true);
              } else {
                setStickyHeader(false);
              }
            }}>
            <HomeHeader
              drawerItem={true}
              onIconPress={() => {
                props.navigation.goBack();
              }}
            />
            <Text style={sendMessageStyles.containerTitle}>Send Message</Text>
            <View
              style={[
                sendMessageStyles.tableFixedHeader,
                {elevation: stickyHeader ? 10 : 8},
              ]}>
              <View style={sendMessageStyles.tableHeader}>
                <Text style={sendMessageStyles.tableHeaderTxt}>
                  Contact list for today's Open Appointment
                </Text>
              </View>
              {isSelectionMode && (
                <Animatable.View
                  animation="lightSpeedIn"
                  style={sendMessageStyles.countColumns}>
                  <Text
                    style={[sendMessageStyles.columnTitle, {color: 'grey'}]}>
                    {selectedPatients.length <= 1
                      ? selectedPatients.length + ' Patient Selected'
                      : selectedPatients.length + ' Patients Selected'}{' '}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      if (!selectAll) {
                        setSelectAll(true);
                        onSelectAll('select');
                      } else {
                        setSelectAll(false);
                        onSelectAll('cancel');
                      }
                    }}>
                    <Text
                      style={[sendMessageStyles.columnTitle, {color: 'gray'}]}>
                      {selectAll ? 'Cancel All' : 'Select All'}
                    </Text>
                  </TouchableOpacity>
                </Animatable.View>
              )}
              <View style={sendMessageStyles.countColumns}>
                <Text style={sendMessageStyles.columnTitle}>Patient Name</Text>

                <Text style={sendMessageStyles.columnTitle}>Contact No</Text>
              </View>
            </View>
            <Animatable.View
              animation="fadeIn"
              style={sendMessageStyles.mainContainer}>
              <FlatList
                data={patientsData}
                ListEmptyComponent={() => (
                  <Text style={sendMessageStyles.containerTitle}>
                    No Appointment Available
                  </Text>
                )}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => {
                      selectItem(item);
                    }}
                    style={[sendMessageStyles.columns, item.selectedClass]}
                    key={index}>
                    <Text style={sendMessageStyles.columnTitle}>
                      {item.patient_name}
                    </Text>

                    <Text style={sendMessageStyles.columnTitle}>
                      {item.patient_number}
                    </Text>
                  </TouchableOpacity>
                )}
                keyExtractor={item => item.id.toString()}
              />
            </Animatable.View>
            {error !== '' && (
              <Animatable.Text
                animation="wobble"
                style={sendMessageStyles.errorTxt}>
                {error}
              </Animatable.Text>
            )}
            <Button
              onPress={() => {
                if (selectedPatients.length === 0) {
                  setError('Please select patients!');
                } else {
                  setError('');
                  sendMessage();
                }
              }}
              title="Message"
              raised
              containerStyle={sendMessageStyles.btn}
              buttonStyle={sendMessageStyles.btnBgClr}
            />
          </ScrollView>
        </SafeAreaView>
      )}
      {isLoading && <LoadingComponent />}
    </>
  );
};

const mapStateToProps = state => ({
  utils: state.utils,
});
const mapDispatchToProps = dispatch => ({
  setAllAppointments: appointments =>
    dispatch(setAllAppointments(appointments)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SendMessage);
