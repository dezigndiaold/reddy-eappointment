import React, {useEffect} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  BackHandler,
  PermissionsAndroid,
  Share,
  ScrollView,
} from 'react-native';
import {Item, Icon, Input, Picker, Thumbnail, Label} from 'native-base';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as Animatable from 'react-native-animatable';
import {createAppointmentStyles} from '../../styles/createAppointmentStyles';
import HomeHeader from '../headers/homeHeader';
import {Button} from 'react-native-elements';
import {BarIndicator, SkypeIndicator} from 'react-native-indicators';
import {
  createAppointment,
  getEveningAvailableTime,
  getMorningAvailableTime,
  getPhoneNumbersList,
} from '../../apis/user-info';
import AsyncStorage from '@react-native-community/async-storage';
import {TouchableOpacity} from 'react-native-gesture-handler';
import footerImage from '../../assets/images/Group-42-3x.png';
import {getAppointments} from '../../apis/user-info';
import {connect} from 'react-redux';
import {setAllAppointments} from '../../redux/actions/utilsActions';
import LoadingComponent from '../loadingComponent';
import moment from 'moment';
import Contacts from 'react-native-contacts';
import morningIcon from '../../assets/images/mrng-icon.png';
import eveningIcon from '../../assets/images/evn-icon.png';

const CreateAppointment = props => {
  const [patientsData, setPateintsData] = React.useState([]);
  const [datePickerValue, setDatePickerValue] = React.useState(new Date());
  const [isDateChange, setDateChange] = React.useState(false);
  const [isTimeChange, setTimeChange] = React.useState(false);
  const [mode, setMode] = React.useState('date');
  const [show, setShow] = React.useState(false);
  const [pateintName, setPatientName] = React.useState('');
  const [date, setDate] = React.useState('');
  const [time, setTime] = React.useState('');
  const [phone, setPhone] = React.useState('');
  const [comment, setComment] = React.useState('');

  const [timeType, setTimeType] = React.useState('');
  const [availableTimes, setAvailableTimes] = React.useState([]);
  const [timeApiStatus, setTimeApiStatus] = React.useState('');
  const [error, setError] = React.useState('');
  const [errorType, setErrorType] = React.useState('');
  const [processingState, setProcessingState] = React.useState('');
  const [isLoading, setLoading] = React.useState(true);
  const [phoneNumbersList, setPhoneNumbersList] = React.useState(null);

  React.useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    getData();

    return () => backHandler.remove();
  }, []);

  const backAction = () => {
    props.navigation.goBack();
    return true;
  };

  async function onSubmit() {
    setErrorType('');
    setError('');
    if (!pateintName) {
      setError("Pantient's name can't be empty!");
      setErrorType('name');
    } else if (!phone || phone.length !== 10) {
      setError('Phone number must be 10 digits!');
      setErrorType('phone');
    } else if (!date) {
      setError("Date can't be empty!");
      setErrorType('date');
    } else if (!time) {
      setError("Time can't be empty!");
      setErrorType('time');
    } else {
      setProcessingState('loading');
      let doctor_data = await AsyncStorage.getItem('userData');
      doctor_data = JSON.parse(doctor_data);

      createAppointment(pateintName, phone, doctor_data.id, date, time, comment)
        .then(res => {
          let mobile = phone.toString();
          PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
            PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
          ])
            .then(() => {
              Contacts.getContactsByPhoneNumber(mobile, (err, res) => {
                if (err) {
                  console.log(err, 'error in finding mobile number ');
                } else {
                  if (res.length === 0) {
                    const newPerson = {
                      phoneNumbers: [
                        {
                          label: 'mobile',
                          number: phone,
                        },
                      ],
                      givenName: pateintName,
                    };
                    Contacts.addContact(newPerson, (err, contact) => {
                      if (err) console.log(err);
                    });
                  }
                }
              });
            })
            .catch(err => {
              console.log(err, 'Permission denied');
            });

          setProcessingState('saved');

          getData();
        })
        .catch(err => {
          console.log(err);
          setError('Error occured');
          setErrorType('server!');
          setProcessingState('');
        });
    }
  }

  function handleAddnew() {
    setPatientName('');
    setDate('');
    setTime('');

    setComment('');
    setPhone('');
    setError('');
    setDateChange(false);
    setTimeChange(false);
    setErrorType('');
    setProcessingState('');
    setDatePickerValue(new Date());
    setTimeType('');
  }
  function formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
  const onDateChange = selectedDate => {
    const currentDate = selectedDate;
    setShow(Platform.OS === 'ios');
    if (selectedDate) {
      setDatePickerValue(currentDate);
      setDate(formatDate(currentDate.toString()));
      setDateChange(true);
    } else {
      setDateChange(false);
    }
    setShow(false);
    setError('');
    setErrorType('');
  };
  function onCancelDate() {
    setDatePickerValue(new Date(date));
    setDate('');
    setDateChange(false);

    setShow(false);
    setError('');
    setErrorType('');
  }

  async function shareDetails() {
    let doctor_data = await AsyncStorage.getItem('userData');
    doctor_data = JSON.parse(doctor_data);
    console.log(doctor_data.id);
    let day = date.slice(-2);
    let month = date.slice(5, 7);
    let year = date.slice(0, 4);

    Share.share({
      title: 'Patient Details',
      message: `Name: ${pateintName}\n\nNumber: ${phone}\n\nAppointment Date: ${day +
        '-' +
        month +
        '-' +
        year}\n\nAppointment Time: ${time}\n\nComments: ${comment}\n\nDoctor's Name: ${
        doctor_data.doctor_name
      }\n\nAppointment Status: Open\n\nShared by eAppointment App
      
      `,
    })
      .then(result => console.log(result))
      .catch(errorMsg => console.log(errorMsg));
  }

  async function getData() {
    let doctor_data = await AsyncStorage.getItem('userData');
    doctor_data = JSON.parse(doctor_data);
    let localAppointments = await AsyncStorage.getItem('allAppointments');
    if (localAppointments) {
      props.setAllAppointments(JSON.parse(localAppointments));
      const allAppointments = JSON.parse(localAppointments);
      const todayDate = moment().format('YYYY-MM-DD');
      let todayAppointments = allAppointments.filter(
        item => item.app_date === todayDate,
      );

      setPateintsData(todayAppointments);
      setLoading(false);
    }
    getAppointments(doctor_data.id)
      .then(res => {
        props.setAllAppointments(res.data);
        const allAppointments = res.data;
        const todayDate = moment().format('YYYY-MM-DD');
        let todayAppointments = allAppointments.filter(
          item => item.app_date === todayDate,
        );
        setPateintsData(todayAppointments);
        AsyncStorage.removeItem('allAppointments');
        AsyncStorage.setItem('allAppointments', JSON.stringify(res.data));
        setLoading(false);
      })
      .catch(async err => {
        let localAppointments = await AsyncStorage.getItem('allAppointments');
        if (localAppointments) {
          props.setAllAppointments(JSON.parse(localAppointments));
          const allAppointments = JSON.parse(localAppointments);
          const todayDate = moment().format('YYYY-MM-DD');

          let todayAppointments = allAppointments.filter(
            item => item.app_date === todayDate,
          );
          setPateintsData(todayAppointments);
        }
        setLoading(false);

        console.log(err);
      });
  }
  async function getAvailableHours(time_type) {
    let doctor_data = await AsyncStorage.getItem('userData');
    doctor_data = JSON.parse(doctor_data);
    setAvailableTimes([]);
    setTimeApiStatus('processing');
    setTimeType(time_type);
    if (time_type === 'morning') {
      setTimeType(time_type);
      getMorningAvailableTime(doctor_data.id, date)
        .then(res => {
          if (res.mrngArray.length === 0) {
            setTimeApiStatus('empty');
          } else {
            setAvailableTimes(res.mrngArray);
            setTime(res.mrngArray[0]);
            setTimeApiStatus('done');
          }
        })
        .catch(err => {
          console.log(err);
          setTimeApiStatus('error');
        });
    } else if (time_type === 'evening') {
      setTimeType(time_type);
      getEveningAvailableTime(doctor_data.id, date)
        .then(res => {
          if (res.EveningArray.length === 0) {
            setTimeApiStatus('empty');
          } else {
            setAvailableTimes(res.EveningArray);
            setTime(res.EveningArray[0]);
            setTimeApiStatus('done');
          }
        })
        .catch(err => {
          console.log(err);

          setTimeApiStatus('error');
        });
    }
  }

  useEffect(() => {
    if (phone.length === 2 || phone.length === 10) {
      setPhoneNumbersList(null);
    }
    const fetchPhoneNumber = async () => {
      const data = await AsyncStorage.getItem('userData');
      getPhoneNumbersList(JSON.parse(data).id, phone)
        .then(res => {
          setPhoneNumbersList(res.data);
        })
        .catch(err => console.warn(err));
    };

    if (phone.length > 2 && phone.length !== 10) {
      fetchPhoneNumber();
    }
  }, [phone]);

  return (
    <>
      {!isLoading && (
        <SafeAreaView>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
            <HomeHeader
              drawerItem={true}
              onIconPress={() => {
                props.navigation.goBack();
              }}
            />
            <Animatable.View
              animation="fadeIn"
              style={createAppointmentStyles.mainContainer}>
              <Text style={createAppointmentStyles.containerTitle}>
                {' '}
                Create Appointment
              </Text>
              <Label style={createAppointmentStyles.itemLabel}>
                Patient Mobile No.
              </Label>
              <Item rounded style={createAppointmentStyles.inputItem}>
                <Input
                  style={createAppointmentStyles.input}
                  placeholderTextColor="gray"
                  keyboardType="phone-pad"
                  maxLength={10}
                  placeholder="Enter Patient Mobile No."
                  value={phone}
                  onChangeText={text => {
                    setPhone(text);
                    setError('');
                    setErrorType('');
                  }}
                />
                {(errorType === 'phone' || errorType === 'credential') && (
                  <Icon
                    active
                    name="close-circle"
                    style={createAppointmentStyles.errorIcon}
                  />
                )}
              </Item>
              {phoneNumbersList && (
                <ScrollView
                  style={createAppointmentStyles.modalWrapper}
                  nestedScrollEnabled
                  keyboardShouldPersistTaps="handled"
                  showsHorizontalScrollIndicator={false}>
                  {phoneNumbersList.map((item, index) => {
                    const {patient_number, patient_name, id} = item;
                    return (
                      <TouchableOpacity
                        activeOpacity={0.6}
                        key={id}
                        onPress={() => {
                          setPhone(patient_number);
                          setPatientName(patient_name);
                          setPhoneNumbersList(null);
                        }}>
                        <Text
                          style={[
                            createAppointmentStyles.phoneNumber,
                            index === phoneNumbersList.length - 1
                              ? {marginBottom: 20}
                              : {},
                          ]}>
                          {item.patient_number}
                        </Text>
                      </TouchableOpacity>
                    );
                  })}
                </ScrollView>
              )}
              <Label style={createAppointmentStyles.itemLabel}>
                Patient Name
              </Label>
              <Item rounded style={createAppointmentStyles.inputItem}>
                <Input
                  style={createAppointmentStyles.input}
                  placeholderTextColor="gray"
                  placeholder="Enter Patient Name"
                  value={pateintName}
                  onChangeText={text => {
                    setPatientName(text);
                    setError('');
                    setErrorType('');
                  }}
                />
                {(errorType === 'name' || errorType === 'credential') && (
                  <Icon
                    active
                    name="close-circle"
                    style={createAppointmentStyles.errorIcon}
                  />
                )}
              </Item>
              <Label style={createAppointmentStyles.itemLabel}>
                Appointment Date
              </Label>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  setError('');
                  setErrorType('');
                  setShow(true);
                }}>
                <Item rounded style={createAppointmentStyles.dateInputItem}>
                  <View>
                    <Text style={createAppointmentStyles.dateInput}>
                      {' '}
                      {isDateChange ? date : 'Enter Date'}
                    </Text>
                  </View>
                  {(errorType === 'date' || errorType === 'credential') && (
                    <Icon
                      active
                      name="close-circle"
                      style={[
                        createAppointmentStyles.errorIcon,
                        {position: 'absolute', right: 0},
                      ]}
                    />
                  )}
                </Item>
              </TouchableOpacity>
              <Label style={createAppointmentStyles.itemLabel}>
                Appointment Time
              </Label>
              {timeType === '' && (
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',

                    flexDirection: 'row',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      if (date) {
                        getAvailableHours('morning');
                      } else {
                        setShow(true);
                      }
                      setError('');
                      setErrorType('');
                      setTimeType('');
                    }}
                    style={createAppointmentStyles.timeIcon}>
                    <>
                      <Thumbnail source={morningIcon} square />
                      <Text style={createAppointmentStyles.timeIconText}>
                        Morning Time
                      </Text>
                    </>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      if (date) {
                        getAvailableHours('evening');
                      } else {
                        setShow(true);
                      }
                      setError('');
                      setErrorType('');
                      setTimeType('');
                    }}
                    style={createAppointmentStyles.timeIcon}>
                    <>
                      <Thumbnail source={eveningIcon} square />
                      <Text style={createAppointmentStyles.timeIconText}>
                        Evening Time
                      </Text>
                    </>
                  </TouchableOpacity>
                </View>
              )}
              {timeType !== '' && availableTimes.length !== 0 && (
                <>
                  <Text style={createAppointmentStyles.availableTimeHeading}>
                    {timeType === 'morning'
                      ? 'Select Morning Available Time'
                      : 'Select Evening Available Time'}
                  </Text>
                  <View style={createAppointmentStyles.pickerContainer}>
                    <Picker
                      note
                      mode="dialog"
                      style={createAppointmentStyles.picker}
                      selectedValue={time}
                      onValueChange={val => {
                        setTime(val);
                      }}>
                      {availableTimes.map((item, index) => (
                        <Picker.Item label={item} value={item} key={index} />
                      ))}
                    </Picker>
                    <Button
                      title="Cancel"
                      buttonStyle={[
                        createAppointmentStyles.timeCanacelBtn,
                        createAppointmentStyles.timeCanacelBtnClr,
                      ]}
                      onPress={() => {
                        setTimeType('');
                        setTime('');
                      }}
                    />
                  </View>
                </>
              )}
              {timeType !== '' && availableTimes.length === 0 && (
                <View>
                  {timeApiStatus === 'processing' ? (
                    <View style={createAppointmentStyles.timeLoader}>
                      <SkypeIndicator
                        size={50}
                        color={createAppointmentStyles.btnBgClr.backgroundColor}
                      />
                      <Text style={createAppointmentStyles.timeLoaderText}>
                        Fetching your avialable time
                      </Text>
                    </View>
                  ) : timeApiStatus === 'error' ? (
                    <Animatable.View
                      animation="wobble"
                      style={createAppointmentStyles.availableTimeError}>
                      <Icon
                        name="timer-off"
                        type="MaterialIcons"
                        style={createAppointmentStyles.availableTimeErrorIcon}
                      />
                      <Text
                        style={createAppointmentStyles.availableTimeErrorrText}>
                        Failed to fetch, try later!
                      </Text>
                    </Animatable.View>
                  ) : timeApiStatus === 'empty' ? (
                    <Animatable.View
                      animation="wobble"
                      style={createAppointmentStyles.availableTimeError}>
                      <Icon
                        name="timer-off"
                        type="MaterialIcons"
                        style={createAppointmentStyles.availableTimeErrorIcon}
                      />
                      <Text
                        style={createAppointmentStyles.availableTimeErrorrText}>
                        No time available in your schedule!
                      </Text>
                    </Animatable.View>
                  ) : (
                    <View />
                  )}
                </View>
              )}
              <DateTimePickerModal
                isVisible={show}
                minimumDate={new Date()}
                testID="dateTimePicker"
                timeZoneOffsetInMinutes={0}
                minuteInterval={5}
                value={datePickerValue}
                mode={'date'}
                is24Hour={true}
                display="default"
                dateFormat={'YYYY-MM-DD'}
                onConfirm={date => {
                  onDateChange(date);
                }}
                onCancel={() => {
                  setShow(false);
                  onCancelDate();
                }}
              />

              <Label style={createAppointmentStyles.itemLabel}>Comment</Label>
              <Item regular style={createAppointmentStyles.commentInputItem}>
                <Input
                  style={[createAppointmentStyles.commentInput]}
                  placeholderTextColor="gray"
                  multiline={true}
                  numberOfLines={4}
                  value={comment}
                  onChangeText={text => {
                    setComment(text);
                    setError('');
                    setErrorType('');
                  }}
                />
                {(errorType === 'comment' || errorType === 'credential') && (
                  <Icon
                    active
                    name="close-circle"
                    style={createAppointmentStyles.errorIcon}
                  />
                )}
              </Item>
              {errorType !== '' && (
                <Animatable.Text
                  animation="wobble"
                  style={createAppointmentStyles.errorTxt}>
                  {error}
                </Animatable.Text>
              )}

              {processingState === '' && (
                <Button
                  title="Submit"
                  raised
                  containerStyle={createAppointmentStyles.btn}
                  buttonStyle={createAppointmentStyles.btnBgClr}
                  onPress={() => onSubmit()}
                />
              )}
              {processingState === 'loading' && (
                <Animatable.View
                  animation="pulse"
                  style={[
                    createAppointmentStyles.submitBtn,
                    createAppointmentStyles.btnBgClr,
                  ]}>
                  <BarIndicator color="white" />
                </Animatable.View>
              )}
              {processingState === 'saved' && (
                <>
                  <View style={createAppointmentStyles.savedContainer}>
                    <Button
                      onPress={() => shareDetails()}
                      title="Share Details"
                      raised
                      containerStyle={createAppointmentStyles.btn}
                      buttonStyle={createAppointmentStyles.btnBgClr}
                    />

                    <Button
                      title="Add new"
                      raised
                      containerStyle={createAppointmentStyles.btn}
                      buttonStyle={createAppointmentStyles.btnBgClr}
                      onPress={() => handleAddnew()}
                    />
                  </View>
                  <Animatable.View
                    animation="rubberBand"
                    style={[
                      createAppointmentStyles.btn,
                      createAppointmentStyles.savedBtnBgClr,
                    ]}>
                    <Text style={createAppointmentStyles.savedTxt}>Saved</Text>
                    <Icon
                      name="done-all"
                      type="MaterialIcons"
                      style={createAppointmentStyles.savedIcon}
                    />
                  </Animatable.View>
                </>
              )}
            </Animatable.View>
            <Thumbnail
              large
              square
              source={footerImage}
              style={createAppointmentStyles.footerImage}
              resizeMode="contain"
            />
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
      {isLoading && <LoadingComponent />}
    </>
  );
};

const mapStateToProps = state => ({
  utils: state.utils,
});
const mapDispatchToProps = dispatch => ({
  setAllAppointments: appointments =>
    dispatch(setAllAppointments(appointments)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateAppointment);
