import React from 'react';
import {Text, TouchableOpacity, View, SafeAreaView} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {loginStyles} from '../../styles/loginStyles';
import {Card, Input, Thumbnail, Item, Icon} from 'native-base';
import {Button} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import AuthHeader from '../headers/authHeader';
import {connect} from 'react-redux';
import {setToken} from '../../redux/actions/utilsActions';
import {BarIndicator} from 'react-native-indicators';
import AsyncStorage from '@react-native-community/async-storage';
import {login} from '../../apis/auth';
import footerImage from '../../assets/images/Group-41-3x.png';

const AuthLogin = props => {
  const [phone, setPhone] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [error, setError] = React.useState('');
  const [errorType, setErrorType] = React.useState('');
  const [processingState, setProcessingState] = React.useState('');

  function onSubmit() {
    if (!phone || phone.length !== 10) {
      setError('Phone number must be 10 digits!');
      setErrorType('phone');
    } else if (!password) {
      setError('Enter your password!');
      setErrorType('password');
    } else {
      setProcessingState('loading');
      login(phone, password)
        .then(async res => {
          const userData = res.data;

          await AsyncStorage.setItem('token', 'user_logged_in');
          await AsyncStorage.setItem('userData', JSON.stringify(userData));

          setProcessingState('');
          props.setToken('user_logged_in');
          setError('');
          setErrorType('');
          setPhone('');
          setPassword('');
          props.navigation.navigate('drawer', {
            screen: 'home',
          });
        })
        .catch(err => {
          setError('Error occured!');
          setErrorType('server!');
          console.log(err);
          setProcessingState('');
        });
    }
  }
  React.useEffect(() => {
    setProcessingState('');
  }, []);
  return (
    <SafeAreaView>
      <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
        <AuthHeader />
        <Animatable.View animation="fadeIn" style={loginStyles.mainContainer}>
          <Text style={loginStyles.appTitle}> E Appointment</Text>
          <Text style={loginStyles.containerTitle}> Login</Text>
          <View style={loginStyles.formContainer}>
            <Item rounded style={loginStyles.inputItem}>
              <Input
                style={loginStyles.input}
                keyboardType="number-pad"
                placeholderTextColor="gray"
                placeholder="Mobile Number"
                maxLength={10}
                value={phone}
                onChangeText={text => {
                  setPhone(text);
                  setError('');
                  setErrorType('');
                }}
              />
              {(errorType === 'phone' || errorType === 'credential') && (
                <Icon
                  active
                  name="close-circle"
                  style={loginStyles.errorIcon}
                />
              )}
            </Item>
            <Item rounded style={loginStyles.inputItem}>
              <Input
                style={loginStyles.input}
                placeholderTextColor="gray"
                placeholder="Password"
                secureTextEntry={true}
                value={password}
                onChangeText={text => {
                  setPassword(text);
                  setError('');
                  setErrorType('');
                }}
              />
              {(errorType === 'password' || errorType === 'credential') && (
                <Icon
                  active
                  name="close-circle"
                  style={loginStyles.errorIcon}
                />
              )}
            </Item>
            {errorType !== '' && (
              <Animatable.Text animation="wobble" style={loginStyles.errorTxt}>
                {error}
              </Animatable.Text>
            )}

            {processingState === '' && (
              <Button
                title="Login"
                raised
                containerStyle={loginStyles.btn}
                buttonStyle={loginStyles.btnBgClr}
                onPress={() => onSubmit()}
              />
            )}
            {processingState === 'loading' && (
              <Animatable.View
                animation="pulse"
                style={[loginStyles.btn, loginStyles.btnBgClr]}>
                <BarIndicator color="white" />
              </Animatable.View>
            )}
            <TouchableOpacity
              onPress={() => {
                setError('');
                setErrorType('');
                setPhone('');
                setPassword('');
                props.navigation.navigate('auth-signup');
              }}>
              <Text style={loginStyles.containerTitle}> Register Now</Text>
            </TouchableOpacity>
          </View>
        </Animatable.View>
        <Thumbnail
          large
          square
          source={footerImage}
          style={loginStyles.footerImage}
          resizeMode="contain"
        />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const mapStateToProps = state => ({
  utils: state.utils,
});
const mapDispatchToProps = dispatch => ({
  setToken: token => dispatch(setToken(token)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AuthLogin);
