import React from 'react';
import {Text, SafeAreaView, TouchableOpacity, View} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {loginStyles} from '../../styles/loginStyles';
import {Card, Input, Item, Icon} from 'native-base';
import {Button} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import AuthHeader from '../headers/authHeader';
import {connect} from 'react-redux';
import {setToken} from '../../redux/actions/utilsActions';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {BarIndicator} from 'react-native-indicators';
import {signUp} from '../../apis/auth';

const AuthSignUp = props => {
  const [name, setName] = React.useState('');
  const [clinic, setCilinic] = React.useState('');
  const [phone, setPhone] = React.useState('');
  const [morningTimeStart, setMorningStart] = React.useState('');
  const [morningTimeEnd, setMorningEnd] = React.useState('');
  const [eveningTimeStart, setEveningStart] = React.useState('');
  const [eveningTimeEnd, setEveningEnd] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [confirmPassword, setConfirmPassword] = React.useState('');

  const [show, setShow] = React.useState(false);
  const [datePickerValue, setDatePickerValue] = React.useState(new Date());
  const [typeoftiming, setTypeoftiming] = React.useState('');
  const [isTimeChange, setTimeChange] = React.useState(true);
  const [error, setError] = React.useState('');
  const [errorType, setErrorType] = React.useState('');
  const [processingState, setProcessingState] = React.useState('');

  function onSubmit() {
    if (!name) {
      setError("Doctor's name can't be empty!");
      setErrorType('name');
    } else if (!clinic) {
      setError("Clinic's name can't be empty!");
      setErrorType('clicnic');
    } else if (!phone || phone.length !== 10) {
      setError('Phone number must be 10 digits!');
      setErrorType('phone');
    } else if (!password || password.length < 8) {
      setError('Password atleast be 8 characters!');
      setErrorType('password');
    } else if (!confirmPassword || password !== confirmPassword) {
      setError('Password not matched!');
      setErrorType('confirmpassword');
    } else {
      setProcessingState('loading');
      signUp(
        name,
        clinic,
        phone,
        password,
        confirmPassword,
        morningTimeStart,
        morningTimeEnd,
        eveningTimeStart,
        eveningTimeEnd,
      )
        .then(res => {
          setProcessingState('saved');
          setTimeout(() => {
            props.navigation.goBack();
          }, 1000);
        })
        .catch(err => {
          setError('Error occured!');
          setErrorType('server');
          setProcessingState('');
          console.log(err);
        });
    }
  }
  const onTimeChange = selectedDate => {
    const currentDate = selectedDate;
    setShow(Platform.OS === 'ios');
    if (selectedDate) {
      setDatePickerValue(currentDate);
      let hour = currentDate.getHours();
      let minutes = currentDate.getMinutes();
      if (hour === 0) {
        hour = '00';
      }
      if (minutes === 0) {
        minutes = '00';
      }
      hour = '0' + hour;
      minutes = '0' + minutes;
      if (typeoftiming === 'morning-start') {
        setMorningStart(hour.slice(-2) + ':' + minutes.slice(-2));
      } else if (typeoftiming === 'morning-end') {
        setMorningEnd(hour.slice(-2) + ':' + minutes.slice(-2));
      } else if (typeoftiming === 'evening-start') {
        setEveningStart(hour.slice(-2) + ':' + minutes.slice(-2));
      } else if (typeoftiming === 'evening-end') {
        setEveningEnd(hour.slice(-2) + ':' + minutes.slice(-2));
      }
      setTimeChange(true);
    } else {
      setTimeChange(false);
    }
    setShow(false);
    setError('');
    setErrorType('');
  };
  function onCancelDate() {
    setDatePickerValue(new Date());
    setMorningStart(morningTimeStart);
    setMorningEnd(morningTimeEnd);
    setEveningStart(eveningTimeStart);
    setEveningEnd(eveningTimeEnd);
    setTimeChange(true);
    setShow(false);
    setError('');
    setErrorType('');
  }
  return (
    <>
      <SafeAreaView>
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
          <AuthHeader />
          <Animatable.View animation="fadeIn" style={loginStyles.mainContainer}>
            <Text style={loginStyles.appTitle}> E Appointment</Text>
            <Text style={loginStyles.containerTitle}> Register Now</Text>
            <View style={loginStyles.formContainer}>
              <Item rounded style={loginStyles.inputItem}>
                <Input
                  style={loginStyles.input}
                  placeholderTextColor="gray"
                  placeholder="Doctor Name"
                  value={name}
                  onChangeText={text => {
                    setName(text);
                    setError('');
                    setErrorType('');
                  }}
                />
                {(errorType === 'name' || errorType === 'credential') && (
                  <Icon
                    active
                    name="close-circle"
                    style={loginStyles.errorIcon}
                  />
                )}
              </Item>
              <Item rounded style={loginStyles.inputItem}>
                <Input
                  style={loginStyles.input}
                  placeholderTextColor="gray"
                  placeholder="Clinic Name"
                  value={clinic}
                  onChangeText={text => {
                    setCilinic(text);
                    setError('');
                    setErrorType('');
                  }}
                />
                {(errorType === 'clinic' || errorType === 'credential') && (
                  <Icon
                    active
                    name="close-circle"
                    style={loginStyles.errorIcon}
                  />
                )}
              </Item>
              <Item rounded style={loginStyles.inputItem}>
                <Input
                  style={loginStyles.input}
                  placeholderTextColor="gray"
                  placeholder="Mobile Number"
                  keyboardType="number-pad"
                  maxLength={10}
                  value={phone}
                  onChangeText={text => {
                    setPhone(text);
                    setError('');
                    setErrorType('');
                  }}
                />

                {(errorType === 'phone' || errorType === 'credential') && (
                  <Icon
                    active
                    name="close-circle"
                    style={loginStyles.errorIcon}
                  />
                )}
              </Item>
              <View style={loginStyles.timingContianer}>
                <Text style={loginStyles.timingHeading}>Clinic Time</Text>

                <View style={loginStyles.timingItem}>
                  <Text style={loginStyles.timingLabel}>Morning</Text>
                  <TouchableOpacity
                    onPress={() => {
                      setTypeoftiming('morning-start');
                      setShow(true);
                    }}>
                    <Input
                      disabled={true}
                      maxLength={5}
                      keyboardType="number-pad"
                      value={morningTimeStart}
                      style={loginStyles.timingInput}
                      onChangeText={text => {
                        setMorningStart(text);
                        setError('');
                        setErrorType('');
                      }}
                    />
                  </TouchableOpacity>
                  <Text style={loginStyles.timingLabel}>to</Text>
                  <TouchableOpacity
                    onPress={() => {
                      setTypeoftiming('morning-end');
                      setShow(true);
                    }}>
                    <Input
                      disabled={true}
                      maxLength={5}
                      keyboardType="number-pad"
                      value={morningTimeEnd}
                      style={loginStyles.timingInput}
                      onChangeText={text => {
                        setMorningEnd(text);
                        setError('');
                        setErrorType('');
                      }}
                    />
                  </TouchableOpacity>
                </View>
                <View style={loginStyles.timingItem}>
                  <Text style={loginStyles.timingLabel}>Evening</Text>
                  <TouchableOpacity
                    onPress={() => {
                      setTypeoftiming('evening-start');
                      setShow(true);
                    }}>
                    <Input
                      disabled={true}
                      maxLength={5}
                      keyboardType="number-pad"
                      value={eveningTimeStart}
                      style={loginStyles.timingInput}
                      onChangeText={text => {
                        setEveningStart(text);
                        setError('');
                        setErrorType('');
                      }}
                    />
                  </TouchableOpacity>
                  <Text style={loginStyles.timingLabel}>to</Text>
                  <TouchableOpacity
                    onPress={() => {
                      setTypeoftiming('evening-end');
                      setShow(true);
                    }}>
                    <Input
                      disabled={true}
                      maxLength={5}
                      keyboardType="number-pad"
                      value={eveningTimeEnd}
                      style={loginStyles.timingInput}
                      onChangeText={text => {
                        setEveningEnd(text);
                        setError('');
                        setErrorType('');
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <Item rounded style={loginStyles.inputItem}>
                <Input
                  style={loginStyles.input}
                  placeholderTextColor="gray"
                  placeholder="Password"
                  secureTextEntry={true}
                  value={password}
                  onChangeText={text => {
                    setPassword(text);
                    setError('');
                    setErrorType('');
                  }}
                />

                {(errorType === 'password' || errorType === 'credential') && (
                  <Icon
                    active
                    name="close-circle"
                    style={loginStyles.errorIcon}
                  />
                )}
              </Item>
              <Item rounded style={loginStyles.inputItem}>
                <Input
                  style={loginStyles.input}
                  placeholderTextColor="gray"
                  placeholder="Confirm Password"
                  secureTextEntry={true}
                  value={confirmPassword}
                  onChangeText={text => {
                    setConfirmPassword(text);
                    setError('');
                    setErrorType('');
                  }}
                />

                {(errorType === 'confirmpassword' ||
                  errorType === 'credential') && (
                  <Icon
                    active
                    name="close-circle"
                    style={loginStyles.errorIcon}
                  />
                )}
              </Item>

              {errorType !== '' && (
                <Animatable.Text
                  animation="wobble"
                  style={loginStyles.errorTxt}>
                  {error}
                </Animatable.Text>
              )}
              {processingState === '' && (
                <Button
                  title="Sign Up"
                  raised
                  containerStyle={loginStyles.btn}
                  buttonStyle={loginStyles.btnBgClr}
                  onPress={() => onSubmit()}
                />
              )}
              {processingState === 'loading' && (
                <Animatable.View
                  animation="pulse"
                  style={[loginStyles.btn, loginStyles.btnBgClr]}>
                  <BarIndicator color="white" />
                </Animatable.View>
              )}
              {processingState === 'saved' && (
                <Animatable.View
                  animation="rubberBand"
                  style={[loginStyles.btn, loginStyles.savedBtnBgClr]}>
                  <Text style={loginStyles.savedTxt}>Saved</Text>
                  <Icon
                    name="done-all"
                    type="MaterialIcons"
                    style={loginStyles.savedIcon}
                  />
                </Animatable.View>
              )}
              <TouchableOpacity onPress={() => props.navigation.goBack()}>
                <Text style={loginStyles.containerTitle}> Go Back</Text>
              </TouchableOpacity>
            </View>
          </Animatable.View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
      <DateTimePickerModal
        isVisible={show}
        minimumDate={new Date()}
        testID="dateTimePicker"
        timeZoneOffsetInMinutes={0}
        minuteInterval={5}
        value={datePickerValue}
        mode={'time'}
        is24Hour={true}
        display="default"
        onConfirm={date => {
          onTimeChange(date);
        }}
        onCancel={() => {
          setShow(false);
          onCancelDate();
        }}
      />
    </>
  );
};

const mapStateToProps = state => ({
  utils: state.utils,
});
const mapDispatchToProps = dispatch => ({
  setToken: token => dispatch(setToken(token)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AuthSignUp);
