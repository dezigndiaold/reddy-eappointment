import React from 'react';
import {
  Text,
  SafeAreaView,
  ScrollView,
  FlatList,
  BackHandler,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {connect} from 'react-redux';

import * as Animatable from 'react-native-animatable';
import {viewAppointmentStyles} from '../../styles/viewAppointmentStyles';
import HomeHeader from '../headers/homeHeader';
import {Icon} from 'native-base';
import LoadingComponent from '../loadingComponent';
import {
  setPatientDetails,
  setAllAppointments,
} from '../../redux/actions/utilsActions';
import {getAppointments} from '../../apis/user-info';
import AsyncStorage from '@react-native-community/async-storage';
import {Button} from 'react-native-elements';

const AnimatableFlatList = Animatable.createAnimatableComponent(FlatList);
const AnimatableBtn = Animatable.createAnimatableComponent(TouchableOpacity);
const AnimatableIcon = Animatable.createAnimatableComponent(Icon);
const ViewAppointment = props => {
  let searchFilterRef = React.useRef();
  let dateFilterRef = React.useRef();
  let statusFilterRef = React.useRef();

  const [filterDay, setFilterDay] = React.useState('');
  const [filterMonth, setFilterMonth] = React.useState('');
  const [filterYear, setFilterYear] = React.useState('');
  const [filterData, setFilterData] = React.useState([]);
  const [isLoading, setLoading] = React.useState(true);
  const [searchPhoneNumber, setSearchPhone] = React.useState('');
  const [isSearchBarExpanded, searchBarExpand] = React.useState(null);
  const [isShowStatusFilters, showStatusFilters] = React.useState(null);
  const [isShowDateFilter, showDateFilter] = React.useState(null);
  const [isSearching, setSearching] = React.useState(false);
  const [isOpenFilter, setOpenFilter] = React.useState(false);
  const [isCancelFilter, setCancelFilter] = React.useState(false);
  const [isCloseFilter, setCloseFilter] = React.useState(false);

  React.useEffect(() => {
    setLoading(true);
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    getData();

    return () => backHandler.remove();
  }, []);

  React.useEffect(() => {
    let data = props.utils.appointments.sort((a, b) => {
      if (a.app_date < b.app_date) return -1;

      if (a.app_date > b.app_date) return 1;

      if (a.app_date == b.app_date) {
        if (a.app_time < b.app_time) return -1;

        if (a.app_time > b.app_time) return 1;

        return 0;
      }
    });

    setFilterData(data.reverse());
  }, [props.utils.appointments]);

  async function getData() {
    let doctor_data = await AsyncStorage.getItem('userData');
    doctor_data = JSON.parse(doctor_data);
    let localAppointments = await AsyncStorage.getItem('allAppointments');
    if (localAppointments) {
      props.setAllAppointments(JSON.parse(localAppointments));
      setLoading(false);
    }
    getAppointments(doctor_data.id)
      .then(res => {
        props.setAllAppointments(res.data);

        AsyncStorage.setItem('allAppointments', JSON.stringify(res.data));
        setLoading(false);
      })
      .catch(async err => {
        let localAppointments = await AsyncStorage.getItem('allAppointments');
        if (localAppointments) {
          props.setAllAppointments(JSON.parse(localAppointments));
        }
        setLoading(false);
        console.log(err);
      });
  }
  const backAction = () => {
    props.navigation.goBack();
    return true;
  };

  function expandSearchBar(isclose) {
    if ((isSearchBarExpanded === null || !isSearchBarExpanded) && !isclose) {
      console.log('f');
      resetFilterStatus();
      resetFilterDate();
      showStatusFilters(null);
      showDateFilter(false);
      searchBarExpand(true);
      dateFilterRef.zoomOut();
      statusFilterRef.zoomOut();
    } else if (isclose) {
      console.log('g');
      setFilterData(props.utils.appointments);
      setSearchPhone('');
      searchBarExpand(false);
    }
  }

  function toggleStatusFilters() {
    if (isShowStatusFilters === null || !isShowStatusFilters) {
      showStatusFilters(true);
    } else {
      setOpenFilter(false);
      setCloseFilter(false);
      setCloseFilter(false);
      showStatusFilters(false);
    }
  }
  function searchFilter(value) {
    value = value.toString().toLowerCase();
    let updateData = props.utils.appointments.filter(
      obj =>
        obj.patient_number.indexOf(value) !== -1 ||
        obj.patient_name.toLowerCase().indexOf(value) !== -1,
    );

    setFilterData(updateData);
  }
  function DateFilter(day, month, year) {
    let updateData = [];
    if (day && !month && !year) {
      updateData = props.utils.appointments.filter(
        obj => obj.app_date.slice(-2) === ('0' + day).slice(-2),
      );
    } else if (!day && month && !year) {
      updateData = props.utils.appointments.filter(
        obj => obj.app_date.slice(5, 7) === ('0' + month).slice(-2),
      );
    } else if (!day && !month && year) {
      updateData = props.utils.appointments.filter(
        obj => obj.app_date.slice(0, 5).indexOf(year) !== -1,
      );
    } else if (day && month && !year) {
      updateData = props.utils.appointments.filter(
        obj =>
          obj.app_date.slice(-2).indexOf(day) !== -1 &&
          obj.app_date.slice(5, 7) === ('0' + month).slice(-2),
      );
    } else if (day && !month && year) {
      updateData = props.utils.appointments.filter(
        obj => obj.app_date.slice(0, 5).indexOf(year) !== -1,
      );
    } else if (!day && month && year) {
      updateData = props.utils.appointments.filter(
        obj =>
          obj.app_date.slice(0, 5).indexOf(year) !== -1 &&
          obj.app_date.slice(5, 7) === ('0' + month).slice(-2),
      );
    } else if (day && month && year) {
      updateData = props.utils.appointments.filter(
        obj => obj.app_date === `${year}-${month}-${day}`,
      );
    } else if (!day && !month && !year) {
      updateData = props.utils.appointments;
    }

    setFilterData(updateData);
  }
  function onStatusFilter(type) {
    const updateData = props.utils.appointments.filter(
      obj => obj.status === type,
    );

    setFilterData(updateData);
  }
  function resetFilterDate() {
    setFilterDay('');
    setFilterMonth('');
    setFilterYear('');
    setFilterData(props.utils.appointments);
  }
  function resetFilterStatus() {
    setOpenFilter(false);
    setCloseFilter(false);
    setCloseFilter(false);
    setFilterData(props.utils.appointments);
  }
  function handleCancelRadio() {
    setCancelFilter(true);
    setOpenFilter(false);
    setCloseFilter(false);
    onStatusFilter('cancelled');
  }
  function handleCloseRadio() {
    setCloseFilter(true);
    setOpenFilter(false);
    setCancelFilter(false);
    onStatusFilter('closed');
  }
  function handleOpenRadio() {
    setCloseFilter(false);
    setOpenFilter(true);
    setCancelFilter(false);
    onStatusFilter('open');
  }
  const expandSearch = {
    from: {
      height: 30,
      width: 110,
    },
    to: {
      height: 50,
      width: 320,
    },
  };
  const collapseSearch = {
    from: {
      height: 50,
      width: 320,
    },
    to: {
      height: 30,
      width: 110,
    },
  };
  const rotateStatusIcon = {
    from: {
      transform: [{rotateZ: '0deg'}],
    },
    to: {
      transform: [{rotateZ: '130deg'}],
    },
  };
  const backStatusIcon = {
    from: {
      transform: [{rotateZ: '130deg'}],
    },
    to: {
      transform: [{rotateZ: '0deg'}],
    },
  };
  return (
    <>
      {!isLoading && (
        <>
          <HomeHeader
            drawerItem={true}
            onIconPress={() => {
              props.navigation.goBack();
            }}
          />

          <Text style={viewAppointmentStyles.containerTitle}>
            View Appointment
          </Text>
          {props.utils.appointments.length !== 0 && (
            <>
              <View style={viewAppointmentStyles.filterContainer}>
                <AnimatableBtn
                  activeOpacity={1}
                  animation={
                    isSearchBarExpanded === true
                      ? expandSearch
                      : isSearchBarExpanded === false
                      ? collapseSearch
                      : null
                  }
                  onPress={() => {
                    if (!isSearchBarExpanded) {
                      expandSearchBar(false);
                    } else {
                      null;
                    }
                  }}
                  ref={ref => (searchFilterRef = ref)}
                  style={[viewAppointmentStyles.searchFilterBtn]}>
                  <Icon
                    name="search"
                    type="MaterialIcons"
                    style={viewAppointmentStyles.searchFilterIcon}
                  />
                  {!isSearchBarExpanded && (
                    <Text style={viewAppointmentStyles.searchFilterBtnTxt}>
                      search
                    </Text>
                  )}
                  {isSearchBarExpanded && (
                    <>
                      <TextInput
                        style={{
                          color: 'grey',
                          fontWeight: 'bold',
                          fontSize: 16,
                        }}
                        value={searchPhoneNumber}
                        onChangeText={text => {
                          searchFilter(text);
                          setSearchPhone(text);
                        }}
                        autoFocus={true}
                      />
                      <View
                        style={{
                          position: 'absolute',
                          paddingRight: 10,
                          alignSelf: 'center',
                          right: 0,
                        }}>
                        <TouchableOpacity
                          onPress={() => {
                            expandSearchBar(true);
                            dateFilterRef.zoomIn();
                            statusFilterRef.zoomIn();
                          }}>
                          <Icon
                            active
                            name="close-circle"
                            style={{
                              fontSize: 22,
                              color: 'grey',
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                    </>
                  )}
                </AnimatableBtn>

                <AnimatableBtn
                  onPress={() => {
                    resetFilterDate();
                    showStatusFilters(null);
                    showDateFilter(!isShowDateFilter);
                    resetFilterStatus();
                  }}
                  ref={ref => (dateFilterRef = ref)}
                  style={viewAppointmentStyles.dateFilterBtn}>
                  <Icon
                    name="calendar-range"
                    type="MaterialCommunityIcons"
                    style={viewAppointmentStyles.dateFilterIcon}
                  />
                  <Text style={viewAppointmentStyles.dateFilterBtnTxt}>
                    Filter By date
                  </Text>
                </AnimatableBtn>
                <AnimatableBtn
                  onPress={() => {
                    resetFilterDate();
                    showDateFilter(false);
                    toggleStatusFilters();
                  }}
                  ref={ref => (statusFilterRef = ref)}
                  style={viewAppointmentStyles.statusFilterBtn}>
                  <AnimatableIcon
                    animation={
                      isShowStatusFilters === true
                        ? rotateStatusIcon
                        : isShowStatusFilters === false
                        ? backStatusIcon
                        : null
                    }
                    name="plus"
                    type="FontAwesome"
                    style={[
                      viewAppointmentStyles.statusFilterIcon,
                      {
                        color: !isShowStatusFilters
                          ? viewAppointmentStyles.btnBgClr.backgroundColor
                          : 'salmon',
                      },
                    ]}
                  />
                  <Text style={viewAppointmentStyles.statusFilterBtnTxt}>
                    status
                  </Text>
                </AnimatableBtn>
              </View>
              <Animatable.View style={viewAppointmentStyles.filterSection}>
                {isShowDateFilter && (
                  <View style={viewAppointmentStyles.dateFilterSection}>
                    <View style={viewAppointmentStyles.dateInputSection}>
                      <TextInput
                        style={viewAppointmentStyles.dayInput}
                        placeholder="DD"
                        maxLength={2}
                        keyboardType="number-pad"
                        value={filterDay}
                        onChangeText={text => {
                          if (
                            text === '' ||
                            text === '0' ||
                            (parseInt(text) > 0 && parseInt(text) <= 31)
                          ) {
                            setFilterDay(text);
                            setSearching(!isSearching);
                          }
                        }}
                        autoFocus={true}
                      />
                      <Text style={viewAppointmentStyles.dateHyphon}>/</Text>
                      <TextInput
                        style={viewAppointmentStyles.monthInput}
                        placeholder="MM"
                        maxLength={2}
                        keyboardType="number-pad"
                        value={filterMonth}
                        onChangeText={text => {
                          if (
                            text === '' ||
                            text === '0' ||
                            (parseInt(text) > 0 && parseInt(text) <= 12)
                          ) {
                            setFilterMonth(text);
                            setSearching(!isSearching);
                          }
                        }}
                      />
                      <Text style={viewAppointmentStyles.dateHyphon}>/</Text>
                      <TextInput
                        style={viewAppointmentStyles.yearInput}
                        placeholder="YYYY"
                        maxLength={4}
                        keyboardType="number-pad"
                        value={filterYear}
                        onChangeText={text => {
                          setFilterYear(text);
                          setSearching(!isSearching);
                        }}
                      />
                    </View>
                    <Button
                      onPress={() =>
                        DateFilter(filterDay, filterMonth, filterYear)
                      }
                      title="Search"
                      buttonStyle={viewAppointmentStyles.dateSearchBtn}
                    />
                  </View>
                )}
                {isShowStatusFilters && (
                  <View style={viewAppointmentStyles.statusFilterSection}>
                    <TouchableOpacity
                      onPress={() => {
                        handleOpenRadio();
                      }}
                      style={viewAppointmentStyles.statusFilterItem}>
                      <Text
                        style={[
                          viewAppointmentStyles.statusFilterItemTxt,
                          {
                            color: 'lightgreen',
                            fontWeight: isOpenFilter ? 'bold' : 'normal',
                          },
                        ]}>
                        Open
                      </Text>
                      <TouchableOpacity
                        underlayColor={'transparent'}
                        onPress={() => handleOpenRadio()}>
                        <View
                          style={[
                            viewAppointmentStyles.customRadio,
                            {
                              borderColor: isOpenFilter
                                ? viewAppointmentStyles.btnBgClr.backgroundColor
                                : 'grey',
                            },
                          ]}>
                          {isOpenFilter && (
                            <View style={viewAppointmentStyles.radioFill} />
                          )}
                        </View>
                      </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        handleCloseRadio();
                      }}
                      style={viewAppointmentStyles.statusFilterItem}>
                      <Text
                        style={[
                          viewAppointmentStyles.statusFilterItemTxt,
                          {
                            color: 'red',
                            fontWeight: isCloseFilter ? 'bold' : 'normal',
                          },
                        ]}>
                        Close
                      </Text>
                      <TouchableOpacity
                        onPress={() => {
                          handleCloseRadio();
                        }}>
                        <View
                          style={[
                            viewAppointmentStyles.customRadio,
                            {
                              borderColor: isCloseFilter
                                ? viewAppointmentStyles.btnBgClr.backgroundColor
                                : 'grey',
                            },
                          ]}>
                          {isCloseFilter && (
                            <View style={viewAppointmentStyles.radioFill} />
                          )}
                        </View>
                      </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        handleCancelRadio();
                      }}
                      style={viewAppointmentStyles.statusFilterItem}>
                      <Text
                        style={[
                          viewAppointmentStyles.statusFilterItemTxt,
                          {
                            color: '#ffc200',
                            fontWeight: isCancelFilter ? 'bold' : 'normal',
                          },
                        ]}>
                        Cancel
                      </Text>
                      <TouchableOpacity
                        onPress={() => {
                          handleCancelRadio();
                        }}>
                        <View
                          style={[
                            viewAppointmentStyles.customRadio,
                            {
                              borderColor: isCancelFilter
                                ? viewAppointmentStyles.btnBgClr.backgroundColor
                                : 'grey',
                            },
                          ]}>
                          {isCancelFilter && (
                            <View style={viewAppointmentStyles.radioFill} />
                          )}
                        </View>
                      </TouchableOpacity>
                    </TouchableOpacity>
                  </View>
                )}
              </Animatable.View>
              <View style={viewAppointmentStyles.columnsHeader}>
                <Text style={viewAppointmentStyles.columnHeaderTitle}>
                  Action
                </Text>

                <Text style={viewAppointmentStyles.columnHeaderTitle}>
                  Name
                </Text>
                <Text style={viewAppointmentStyles.columnHeaderTitle}>
                  Mobile No.
                </Text>
                <Text style={viewAppointmentStyles.columnHeaderTitle}>
                  Date Time
                </Text>
              </View>

              <AnimatableFlatList
                animation="fadeInUp"
                delay={500}
                showsVerticalScrollIndicator={false}
                data={filterData}
                ListEmptyComponent={() => (
                  <Text style={viewAppointmentStyles.containerTitle}>
                    No result found
                  </Text>
                )}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    key={index}
                    style={viewAppointmentStyles.columns}
                    onPress={() => {
                      props.setPatientDetails(item);
                      props.navigation.navigate('viewpatientdetails');
                    }}>
                    <Icon
                      name="eye"
                      type="FontAwesome5"
                      style={viewAppointmentStyles.columnIcon}
                    />
                    <Text style={[viewAppointmentStyles.columnTitle]}>
                      {item.patient_name}
                    </Text>

                    <Text style={viewAppointmentStyles.columnTitle}>
                      {item.patient_number}
                    </Text>
                    <Text
                      style={[
                        viewAppointmentStyles.columnTitle,
                        {textAlign: 'right'},
                      ]}>
                      {item.app_date + ' ' + item.app_time}
                    </Text>
                  </TouchableOpacity>
                )}
                keyExtractor={item => item.id.toString()}
              />
            </>
          )}
          {props.utils.appointments.length === 0 && (
            <Text style={viewAppointmentStyles.containerTitle}>
              No Appointment Available
            </Text>
          )}
        </>
      )}
      {isLoading && <LoadingComponent />}
    </>
  );
};

const mapStateToProps = state => ({
  utils: state.utils,
});
const mapDispatchToProps = dispatch => ({
  setPatientDetails: patientDetails =>
    dispatch(setPatientDetails(patientDetails)),
  setAllAppointments: appointments =>
    dispatch(setAllAppointments(appointments)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ViewAppointment);
