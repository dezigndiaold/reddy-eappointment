import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  BackHandler,
  Platform,
  Alert,
  PermissionsAndroid,
  Picker,
  Share,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Item, Icon, Input, Thumbnail, Label} from 'native-base';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as Animatable from 'react-native-animatable';
import {createAppointmentStyles} from '../../styles/createAppointmentStyles';
import HomeHeader from '../headers/homeHeader';
import {Button} from 'react-native-elements';
import {BarIndicator, SkypeIndicator} from 'react-native-indicators';
import {connect} from 'react-redux';
import {
  setPatientDetails,
  setAllAppointments,
} from '../../redux/actions/utilsActions';
import {
  updateAppointment,
  closeAppointment,
  cancelAppointment,
  getEveningAvailableTime,
  getMorningAvailableTime,
} from '../../apis/user-info';

import AsyncStorage from '@react-native-community/async-storage';
import Contacts from 'react-native-contacts';
import morningIcon from '../../assets/images/mrng-icon.png';
import eveningIcon from '../../assets/images/evn-icon.png';

const ViewPateintDetails = props => {
  const [mode, setMode] = React.useState('');
  const [show, setShow] = React.useState(false);
  const [datePickerValue, setDatePickerValue] = React.useState(new Date());
  const [isDateChange, setDateChange] = React.useState(true);

  const [pateintName, setPatientName] = React.useState('');
  const [date, setDate] = React.useState('');
  const [time, setTime] = React.useState('');
  const [phone, setPhone] = React.useState('');
  const [comment, setComment] = React.useState('');
  const [status, setStatus] = React.useState('');

  const [changeTime, setChangeTime] = React.useState(false);
  const [timeType, setTimeType] = React.useState('');
  const [availableTimes, setAvailableTimes] = React.useState([]);
  const [timeApiStatus, setTimeApiStatus] = React.useState('');
  const [canEdit, toggleCanEdit] = React.useState(false);
  const [error, setError] = React.useState('');
  const [errorType, setErrorType] = React.useState('');
  const [processingState, setProcessingState] = React.useState('');

  React.useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    const patientDetails = props.utils.patientDetails;
    setPatientName(patientDetails.patient_name);
    setDate(patientDetails.app_date);
    setTime(patientDetails.app_time);
    setPhone(patientDetails.patient_number);
    setComment(patientDetails.comments);
    setStatus(patientDetails.status);
    setDatePickerValue(new Date());

    return () => backHandler.remove();
  }, []);

  const backAction = () => {
    props.navigation.goBack();
    return true;
  };

  async function onSubmit() {
    setError('');
    setErrorType('');
    if (!pateintName) {
      setError("Pantient's name can't be empty!");
      setErrorType('name');
    } else if (!phone || phone.length !== 10) {
      setError('Phone number must be 10 digits!');
      setErrorType('phone');
    } else if (!date) {
      setError("Date can't be empty!");
      setErrorType('date');
    } else if (!time) {
      setError("Time can't be empty!");
      setErrorType('date');
    } else {
      setProcessingState('loading');
      const patientDetails = props.utils.patientDetails;
      let doctor_data = await AsyncStorage.getItem('userData');
      doctor_data = JSON.parse(doctor_data);
      updateAppointment(
        patientDetails.id,
        doctor_data.id,
        pateintName,
        phone,
        date,
        time,
        comment,
      )
        .then(res => {
          setProcessingState('saved');
          toggleCanEdit(false);
          setTimeType('');
          setChangeTime(false);
          let newData = props.utils.appointments;
          let objPateintDetailIndex = newData.findIndex(
            obj => obj.id == patientDetails.id,
          );
          newData[objPateintDetailIndex] = {
            ...patientDetails,
            patient_name: pateintName,
            patient_number: phone,
            app_date: date,
            app_time: time,
            comments: comment,
          };
          props.setAllAppointments(newData);
          props.setPatientDetails(newData[objPateintDetailIndex]);
          AsyncStorage.setItem('allAppointments', JSON.stringify(newData));

          let mobile = phone.toString();
          PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
            PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
          ])
            .then(() => {
              Contacts.getContactsByPhoneNumber(mobile, (err, res) => {
                if (err) {
                  console.log(err, 'error in finding mobile number ');
                } else {
                  if (res.length === 0) {
                    const newPerson = {
                      phoneNumbers: [
                        {
                          label: 'mobile',
                          number: phone,
                        },
                      ],
                      givenName: pateintName,
                    };
                    Contacts.addContact(newPerson, (err, contact) => {
                      if (err) console.log(err);
                    });
                  }
                }
              });
            })
            .catch(err => {
              console.log(err, 'pemission denied');
            });
        })

        .catch(err => {
          setProcessingState('');
          setErrorType('server');
          setError('Error occured!');
          console.log(
            err,
            patientDetails.id,
            doctor_data.id,
            pateintName,
            phone,
            date,
            time,
            comment,
          );
        });
    }
  }

  function handleEdit() {
    if (processingState !== '') setProcessingState('');
    toggleCanEdit(!canEdit);
    if (processingState !== 'saved') {
      const patientDetails = props.utils.patientDetails;
      setPatientName(patientDetails.patient_name);
      setDate(patientDetails.app_date);
      setTime(patientDetails.app_time);
      setPhone(patientDetails.patient_number);
      setComment(patientDetails.comments);
      setStatus(patientDetails.status);
      setDatePickerValue(new Date());
      setTimeType('');
      setChangeTime(false);
    }
    setError('');
    setErrorType('');
  }
  async function shareDetails() {
    const patient = props.utils.patientDetails;
    let doctor_data = await AsyncStorage.getItem('userData');
    doctor_data = JSON.parse(doctor_data);
    let date = patient.app_date;
    let day = date.slice(-2);
    let month = date.slice(5, 7);
    let year = date.slice(0, 4);

    Share.share({
      title: 'Patient Details',
      message: `Name: ${patient.patient_name}\n\nNumber: ${
        patient.patient_number
      }\n\nAppointment Date: ${day +
        '-' +
        month +
        '-' +
        year}\n\nAppointment Time: ${patient.app_time}\n\nComments: ${
        patient.comments
      }\n\nDoctor's Name: ${doctor_data.doctor_name}\n\nAppointment Status: ${
        patient.status
      }\n\nShared by eAppointment App
      
      `,
    })
      .then(result => console.log(result))
      .catch(errorMsg => console.log(errorMsg));
  }
  function formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
  const onDateChange = selectedDate => {
    const currentDate = selectedDate;
    setShow(Platform.OS === 'ios');
    if (selectedDate) {
      setDatePickerValue(currentDate);
      setDate(formatDate(currentDate.toString()));
      setDateChange(true);
    } else {
      setDateChange(false);
    }
    setShow(false);
    setError('');
    setErrorType('');
  };
  function onCancelDate() {
    setDatePickerValue(new Date(date));
    setDate(date);
    setDateChange(true);

    setShow(false);
    setError('');
    setErrorType('');
  }

  function statusUpdation(action) {
    setError('');
    setErrorType('');
    Alert.alert('Alert', `This appointment will be ${action.toLowerCase()}.`, [
      {
        text: 'Ok',
        onPress: () => {
          updateStatus(action);
        },
      },
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
    ]);
  }
  async function updateStatus(action) {
    const patientDetails = props.utils.patientDetails;
    let doctor_data = await AsyncStorage.getItem('userData');
    doctor_data = JSON.parse(doctor_data);
    if (action === 'cancelled') {
      cancelAppointment(patientDetails.id, doctor_data.id, action)
        .then(res => {
          toggleCanEdit(false);
          setPatientName(patientDetails.patient_name);
          setDate(patientDetails.app_date);
          setTime(patientDetails.app_time);
          setPhone(patientDetails.patient_number);
          setComment(patientDetails.comments);
          setStatus(action);
          let newData = props.utils.appointments;
          let objPateintDetailIndex = newData.findIndex(
            obj => obj.id == patientDetails.id,
          );
          newData[objPateintDetailIndex] = {
            ...patientDetails,
            status: action,
          };
          props.setAllAppointments(newData);
          props.setPatientDetails(newData[objPateintDetailIndex]);
          AsyncStorage.setItem('allAppointments', JSON.stringify(newData));
          setChangeTime(false);
        })
        .catch(err => {
          console.log(err);
          setError('Failed to reject the status!');
          setErrorType('server');
        });
    } else if (action === 'closed') {
      closeAppointment(patientDetails.id, doctor_data.id, action)
        .then(res => {
          console.log(res);
          toggleCanEdit(false);
          setPatientName(patientDetails.patient_name);
          setDate(patientDetails.app_date);
          setTime(patientDetails.app_time);
          setPhone(patientDetails.patient_number);
          setComment(patientDetails.comments);
          setStatus(action);
          let newData = props.utils.appointments;
          let objPateintDetailIndex = newData.findIndex(
            obj => obj.id == patientDetails.id,
          );
          newData[objPateintDetailIndex] = {
            ...patientDetails,
            status: action,
          };
          props.setAllAppointments(newData);
          props.setPatientDetails(newData[objPateintDetailIndex]);
          AsyncStorage.setItem('allAppointments', JSON.stringify(newData));
          const hour = parseInt(patientDetails.patient_number.slice(2));
          setChangeTime(false);
        })
        .catch(err => {
          console.log(err);
          setError('Failed to close the status!');
          setErrorType('server');
        });
    }
  }

  async function getAvailableHours(time_type, date) {
    let doctor_data = await AsyncStorage.getItem('userData');
    doctor_data = JSON.parse(doctor_data);
    setAvailableTimes([]);
    setTimeApiStatus('processing');
    setTimeType(time_type);
    if (time_type === 'morning') {
      setTimeType(time_type);
      getMorningAvailableTime(doctor_data.id, date)
        .then(res => {
          if (res.mrngArray.length === 0) {
            setTimeApiStatus('empty');
          } else {
            setTime(res.mrngArray[0]);
            setAvailableTimes(res.mrngArray);
            setTimeApiStatus('done');
          }
        })
        .catch(err => {
          console.log(err);
          setTimeApiStatus('error');
        });
    } else if (time_type === 'evening') {
      setTimeType(time_type);
      getEveningAvailableTime(doctor_data.id, date)
        .then(res => {
          if (res.EveningArray.length === 0) {
            setTimeApiStatus('empty');
          } else {
            setTime(res.EveningArray[0]);
            setAvailableTimes(res.EveningArray);
            setTimeApiStatus('done');
          }
        })
        .catch(err => {
          console.log(err);

          setTimeApiStatus('error');
        });
    }
  }

  return (
    <SafeAreaView>
      <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
        <HomeHeader
          drawerItem={true}
          onIconPress={() => {
            props.navigation.goBack();
          }}
        />
        <Animatable.View
          animation="fadeIn"
          style={createAppointmentStyles.mainContainer}>
          <Text style={createAppointmentStyles.containerTitle}>
            {' '}
            View Patient Details
          </Text>
          {processingState !== 'loading' && (
            <View style={createAppointmentStyles.editIconContainer}>
              <TouchableOpacity
                style={createAppointmentStyles.editBtn}
                onPress={() => handleEdit()}>
                <Icon
                  name={canEdit ? 'pencil-remove-outline' : 'pencil-outline'}
                  type="MaterialCommunityIcons"
                  style={createAppointmentStyles.editIcon}
                />
                {processingState === '' && !canEdit && (
                  <Text style={createAppointmentStyles.editIconTxt}>
                    Can Edit
                  </Text>
                )}
                {processingState === 'saved' && !canEdit && (
                  <Text style={createAppointmentStyles.editIconTxt}>
                    Edit Again
                  </Text>
                )}
                {canEdit && (
                  <Text style={createAppointmentStyles.editIconTxt}>
                    Cancel Edit
                  </Text>
                )}
              </TouchableOpacity>
            </View>
          )}
          <Label style={createAppointmentStyles.itemLabel}>Patient Name</Label>
          <Item rounded style={createAppointmentStyles.inputItem}>
            <Input
              disabled={!canEdit}
              style={createAppointmentStyles.input}
              placeholderTextColor="gray"
              placeholder="Enter Patient Name"
              value={pateintName}
              onChangeText={text => {
                setPatientName(text);
                setError('');
                setErrorType('');
              }}
            />
            {(errorType === 'name' || errorType === 'credential') && (
              <Icon
                active
                name="close-circle"
                style={createAppointmentStyles.errorIcon}
              />
            )}
          </Item>
          <Label style={createAppointmentStyles.itemLabel}>
            Patient Mobile No.
          </Label>
          <Item rounded style={createAppointmentStyles.inputItem}>
            <Input
              disabled={!canEdit}
              style={createAppointmentStyles.input}
              placeholderTextColor="gray"
              keyboardType="phone-pad"
              maxLength={10}
              placeholder="Enter Patient Mobile No."
              value={phone}
              onChangeText={text => {
                setPhone(text);
                setError('');
                setErrorType('');
              }}
            />
            {(errorType === 'phone' || errorType === 'credential') && (
              <Icon
                active
                name="close-circle"
                style={createAppointmentStyles.errorIcon}
              />
            )}
          </Item>

          <Label style={createAppointmentStyles.itemLabel}>
            Appointment Date
          </Label>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => {
              if (canEdit) setShow(true);
            }}>
            <Item rounded style={createAppointmentStyles.dateInputItem}>
              <View>
                <Text style={createAppointmentStyles.dateInput}>
                  {' '}
                  {isDateChange ? date : 'Enter Date'}
                </Text>
              </View>
              {(errorType === 'date' || errorType === 'credential') && (
                <Icon
                  active
                  name="close-circle"
                  style={[
                    createAppointmentStyles.errorIcon,
                    {position: 'absolute', right: 0},
                  ]}
                />
              )}
            </Item>
          </TouchableOpacity>

          <Label style={createAppointmentStyles.itemLabel}>
            Appointment Time
          </Label>

          {!changeTime && (
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => {
                if (canEdit && !changeTime) {
                  setChangeTime(true);
                  setError('');
                  setErrorType('');
                }
              }}>
              <Item rounded style={[createAppointmentStyles.timeInputfield]}>
                <Text style={createAppointmentStyles.dateInput}> {time}</Text>
              </Item>
            </TouchableOpacity>
          )}
          {timeType === '' && canEdit && changeTime && (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',

                flexDirection: 'row',
              }}>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  if (!canEdit) {
                    return;
                  } else if (date) {
                    getAvailableHours('morning', date);
                  } else {
                    setShow(true);
                  }
                  setError('');
                  setErrorType('');
                  setTimeType('');
                }}
                style={createAppointmentStyles.timeIcon}>
                <>
                  <Thumbnail source={morningIcon} square />
                  <Text style={createAppointmentStyles.timeIconText}>
                    Morning Time
                  </Text>
                </>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  if (!canEdit) {
                    return;
                  } else if (date) {
                    getAvailableHours('evening', date);
                  } else {
                    setShow(true);
                  }
                  setError('');
                  setErrorType('');
                  setTimeType('');
                }}
                style={createAppointmentStyles.timeIcon}>
                <>
                  <Thumbnail source={eveningIcon} square />
                  <Text style={createAppointmentStyles.timeIconText}>
                    Evening Time
                  </Text>
                </>
              </TouchableOpacity>
            </View>
          )}

          {changeTime && timeType !== '' && availableTimes.length !== 0 && (
            <>
              <Text style={createAppointmentStyles.availableTimeHeading}>
                {timeType === 'morning'
                  ? 'Select Morning Available Time'
                  : 'Select Evening Available Time'}
              </Text>
              <View style={createAppointmentStyles.pickerContainer}>
                <Picker
                  note
                  mode="dialog"
                  style={[createAppointmentStyles.picker, {flexGrow: 1}]}
                  selectedValue={time}
                  itemStyle={{color: 'grey'}}
                  onValueChange={val => setTime(val)}>
                  {availableTimes.map((item, index) => (
                    <Picker.Item label={item} value={item} key={index} />
                  ))}
                </Picker>
                <Button
                  buttonStyle={[
                    createAppointmentStyles.timeCanacelBtn,
                    createAppointmentStyles.timeCanacelBtnClr,
                  ]}
                  title="Cancel"
                  onPress={() => {
                    setChangeTime(false);
                    setTimeType('');
                    setTime(props.utils.patientDetails.app_time);
                  }}
                />
              </View>
            </>
          )}

          {changeTime && timeType !== '' && availableTimes.length === 0 && (
            <View>
              {timeApiStatus === 'processing' ? (
                <View style={createAppointmentStyles.timeLoader}>
                  <SkypeIndicator
                    size={50}
                    color={createAppointmentStyles.btnBgClr.backgroundColor}
                  />
                  <Text style={createAppointmentStyles.timeLoaderText}>
                    Fetching your avialable time
                  </Text>
                </View>
              ) : timeApiStatus === 'error' ? (
                <Animatable.View
                  animation="wobble"
                  style={createAppointmentStyles.availableTimeError}>
                  <Icon
                    name="timer-off"
                    type="MaterialIcons"
                    style={createAppointmentStyles.availableTimeErrorIcon}
                  />
                  <Text style={createAppointmentStyles.availableTimeErrorrText}>
                    Failed to fetch, try later!
                  </Text>
                </Animatable.View>
              ) : timeApiStatus === 'empty' ? (
                <Animatable.View
                  animation="wobble"
                  style={createAppointmentStyles.availableTimeError}>
                  <Icon
                    name="timer-off"
                    type="MaterialIcons"
                    style={createAppointmentStyles.availableTimeErrorIcon}
                  />
                  <Text style={createAppointmentStyles.availableTimeErrorrText}>
                    No time available in your schedule!
                  </Text>
                </Animatable.View>
              ) : (
                <Text />
              )}
            </View>
          )}
          <DateTimePickerModal
            isVisible={show}
            minimumDate={new Date()}
            testID="dateTimePicker"
            timeZoneOffsetInMinutes={0}
            minuteInterval={5}
            value={datePickerValue}
            mode={'date'}
            is24Hour={true}
            display="default"
            dateFormat={'YYYY-MM-DD'}
            onConfirm={date => {
              onDateChange(date);
            }}
            onCancel={() => {
              setShow(false);
              onCancelDate();
            }}
          />

          <Label style={createAppointmentStyles.itemLabel}>Comment</Label>
          <Item regular style={createAppointmentStyles.commentInputItem}>
            <Input
              disabled={!canEdit}
              style={createAppointmentStyles.commentInput}
              placeholderTextColor="gray"
              multiline={true}
              numberOfLines={4}
              value={comment}
              onChangeText={text => {
                setComment(text);
                setError('');
                setErrorType('');
              }}
            />
            {(errorType === 'comment' || errorType === 'credential') && (
              <Icon
                active
                name="close-circle"
                style={createAppointmentStyles.errorIcon}
              />
            )}
          </Item>
          <View style={createAppointmentStyles.statusContainer}>
            <View style={createAppointmentStyles.currentStatusContainer}>
              <Text style={createAppointmentStyles.currentStatusLabel}>
                Current Status:
              </Text>
              <Text
                style={[
                  createAppointmentStyles.currentStatusLabel,
                  {
                    color:
                      status === 'cancelled'
                        ? 'red'
                        : status === 'closed'
                        ? '#ffc200'
                        : createAppointmentStyles.btnBgClr.backgroundColor,
                  },
                ]}>
                {' ' + status}
              </Text>
            </View>
            <View style={createAppointmentStyles.currentStatusBtnContainer}>
              <TouchableOpacity
                onPress={() => {
                  statusUpdation('cancelled');
                }}
                style={createAppointmentStyles.currentStatusBtn}>
                {status === 'cancelled' && (
                  <Icon
                    active
                    name="close-circle"
                    style={createAppointmentStyles.errorIcon}
                  />
                )}
                {status !== 'cancelled' && (
                  <Icon
                    type="Ionicons"
                    android="md-close-circle-outline"
                    ios="ios-close-circle-outline"
                    style={createAppointmentStyles.errorIcon}
                  />
                )}
                <Text style={createAppointmentStyles.currentStatusBtnLabel}>
                  Cancel Appointment
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  statusUpdation('closed');
                }}
                style={createAppointmentStyles.currentStatusBtn}>
                {status === 'closed' && (
                  <Icon
                    active
                    name="close-circle"
                    style={createAppointmentStyles.cancelIcon}
                  />
                )}
                {status !== 'closed' && (
                  <Icon
                    type="Ionicons"
                    android="md-close-circle-outline"
                    ios="ios-close-circle-outline"
                    style={createAppointmentStyles.cancelIcon}
                  />
                )}
                <Text style={createAppointmentStyles.currentStatusBtnLabel}>
                  Close Appointment
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          {errorType !== '' && (
            <Animatable.Text
              animation="wobble"
              style={createAppointmentStyles.errorTxt}>
              {error}
            </Animatable.Text>
          )}

          {processingState === '' && canEdit && (
            <View style={createAppointmentStyles.savedContainer}>
              <Button
                title="Submit"
                raised
                containerStyle={createAppointmentStyles.btn}
                buttonStyle={createAppointmentStyles.btnBgClr}
                onPress={() => onSubmit()}
              />
            </View>
          )}
          {processingState === '' && !canEdit && (
            <View style={createAppointmentStyles.savedContainer}>
              <Button
                onPress={() => shareDetails()}
                title="Share Details"
                raised
                containerStyle={createAppointmentStyles.btn}
                buttonStyle={createAppointmentStyles.btnBgClr}
              />
            </View>
          )}
          {processingState === 'loading' && (
            <Animatable.View
              animation="pulse"
              style={[
                createAppointmentStyles.submitBtn,
                createAppointmentStyles.btnBgClr,
              ]}>
              <BarIndicator color="white" />
            </Animatable.View>
          )}
          {processingState === 'saved' && (
            <View style={createAppointmentStyles.savedContainer}>
              <Animatable.View
                animation="rubberBand"
                style={[
                  createAppointmentStyles.btn,
                  createAppointmentStyles.savedBtnBgClr,
                ]}>
                <Text style={createAppointmentStyles.savedTxt}>Saved</Text>
                <Icon
                  name="done-all"
                  type="MaterialIcons"
                  style={createAppointmentStyles.savedIcon}
                />
              </Animatable.View>
              <Button
                onPress={() => shareDetails()}
                title="Share Details"
                raised
                containerStyle={createAppointmentStyles.btn}
                buttonStyle={createAppointmentStyles.btnBgClr}
              />
            </View>
          )}
        </Animatable.View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
const mapStateToProps = state => ({
  utils: state.utils,
});
const mapDispatchToProps = dispatch => ({
  setPatientDetails: patientDetails =>
    dispatch(setPatientDetails(patientDetails)),
  setAllAppointments: appointments =>
    dispatch(setAllAppointments(appointments)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ViewPateintDetails);
