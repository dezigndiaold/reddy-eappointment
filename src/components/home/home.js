import React from 'react';
import {
  View,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Text,
  BackHandler,
  Alert,
  RefreshControl,
  Animated,
  Image,
  Dimensions,
  FlatList,
} from 'react-native';
import {connect} from 'react-redux';
import {BarIndicator} from 'react-native-indicators';
import {Card} from 'native-base';
import * as Animatable from 'react-native-animatable';
import {homeStyles} from '../../styles/homeStyles';
import HomeHeader from '../headers/homeHeader';
import {Button} from 'react-native-elements';
import {getAppointmentCount, getBannerList} from '../../apis/user-info';
import AsyncStorage from '@react-native-community/async-storage';
import LoadingComponent from '../loadingComponent';

let AnimatableCircle = Animatable.createAnimatableComponent(Card);

const {width, height} = Dimensions.get('window');

const Home = props => {
  let flatList = React.useRef();

  const [refreshing, setRefreshing] = React.useState(false);
  const [isLoading, setLoading] = React.useState(false);
  // const [newscreenWidth, setScreenWidth] = React.useState(screenWidth);
  const [appointmentCounts, setAppointmentCounts] = React.useState(null);
  const [images, setImages] = React.useState([]);
  const [activeImageIndex, setActiveImageIndex] = React.useState(0);
  React.useEffect(() => {
    //setLoading(true); initial loader
    BackHandler.addEventListener('hardwareBackPress', backAction);

    getUserData();
    fetchSliderImages();
    return () =>
      BackHandler.removeEventListener('hardwareBackPress', backAction);
  }, []);

  const scrollX = new Animated.Value(0);
  let position = Animated.divide(scrollX, width);

  async function getUserData() {
    const data = await AsyncStorage.getItem('userData');
    const localAppointmentCounts = await AsyncStorage.getItem(
      'appointmentCounts',
    );

    setAppointmentCounts(localAppointmentCounts);
    setLoading(false);
    fetchAppointmentCounts(JSON.parse(data).id);
  }

  function fetchAppointmentCounts(doctor_id) {
    getAppointmentCount(doctor_id)
      .then(res => {
        const allAppoinment = res.data.all_appoinment[0].allAppoinment;
        const todayAppointment =
          res.data.appointmentByDate[0].appointmentByDate;
        const newAppointmentCounts = {
          allAppointments: allAppoinment,
          todayAppointments: todayAppointment,
        };
        AsyncStorage.setItem(
          'appointmentCounts',
          JSON.stringify(newAppointmentCounts),
        );
        setAppointmentCounts(JSON.stringify(newAppointmentCounts));
        setLoading(false);
      })
      .catch(err => {
        console.log(err);
        setLoading(false);
      });
  }
  function fetchSliderImages() {
    getBannerList()
      .then(res => {
        setImages(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }
  const backAction = () => {
    Alert.alert('Hold on! ', 'Are you sure you want to close the app?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {text: 'YES', onPress: () => BackHandler.exitApp()},
    ]);
    return true;
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getUserData();
    wait(2000).then(() => setRefreshing(false));
  }, [refreshing]);

  function wait(timeout) {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  }

  const onViewRef = React.useRef(({viewableItems, changed}) => {
    setActiveImageIndex(viewableItems[0].index);
  });
  const viewConfigRef = React.useRef({temVisiblePercentThreshold: 90});

  return (
    <>
      {!isLoading && (
        <SafeAreaView>
          <ScrollView
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }>
            <HomeHeader
              drawerItem={false}
              onIconPress={() => {
                BackHandler.removeEventListener(
                  'hardwareBackPress',
                  backAction,
                );
                props.navigation.toggleDrawer();
              }}
            />

            <View style={homeStyles.btnGroup}>
              <Button
                title="Create Appointment"
                raised
                containerStyle={homeStyles.createBtn}
                buttonStyle={homeStyles.btnBgClr}
                onPress={() => props.navigation.navigate('createappointment')}
              />
              <Button
                title="View Appointment"
                raised
                containerStyle={homeStyles.viewBtn}
                buttonStyle={homeStyles.btnBgClr}
                onPress={() => props.navigation.navigate('viewappointment')}
              />
            </View>

            <View style={homeStyles.appointmentContainer}>
              <Text style={homeStyles.appointmentLabel}>
                Total Appointments
              </Text>
              <AnimatableCircle
                delay={1000}
                animation="zoomIn"
                style={homeStyles.outerCirlce}>
                <View style={homeStyles.innerCircle}>
                  <Text style={homeStyles.appointmentNumber}>
                    {appointmentCounts
                      ? JSON.parse(appointmentCounts).allAppointments
                      : 0}
                  </Text>
                </View>
              </AnimatableCircle>
            </View>

            <View style={homeStyles.appointmentContainer}>
              <Text style={homeStyles.appointmentLabel}>
                Today's Appointments
              </Text>
              <AnimatableCircle
                delay={1000}
                animation="zoomIn"
                style={homeStyles.outerCirlce}>
                <View style={homeStyles.innerCircle}>
                  <Text style={homeStyles.appointmentNumber}>
                    {appointmentCounts
                      ? JSON.parse(appointmentCounts).todayAppointments
                      : 0}
                  </Text>
                </View>
              </AnimatableCircle>
            </View>
            {images.length !== 0 && (
              <>
                <FlatList
                  data={images.filter(item => item.deleted !== '1')}
                  ref={flatList}
                  keyExtractor={(item, index) => 'key' + index}
                  horizontal
                  pagingEnabled
                  scrollEnabled
                  snapToAlignment="center"
                  scrollEventThrottle={16}
                  decelerationRate={'fast'}
                  showsHorizontalScrollIndicator={false}
                  renderItem={({item}) => {
                    return (
                      <View
                        style={[
                          {
                            width: width - 20,
                            height: height / 3,
                          },
                          homeStyles.carouselItem,
                        ]}>
                        <Image
                          style={{
                            width: width - 20,
                            height: height / 3,
                            borderRadius: 10,
                          }}
                          resizeMode={'stretch'}
                          source={{uri: item.banner_img}}
                        />
                      </View>
                    );
                  }}
                  onScroll={Animated.event(
                    [{nativeEvent: {contentOffset: {x: scrollX}}}],
                    true,
                  )}
                />
                <View style={homeStyles.dotsContainer}>
                  {images
                    .filter(item => item.deleted !== '1')
                    .map((_, i) => {
                      let opacity = position.interpolate({
                        inputRange: [i - 1, i, i + 1],
                        outputRange: [0.3, 1, 0.3],
                        extrapolate: 'clamp',
                      });
                      return (
                        <Animated.View
                          useNativeDriver={true}
                          key={i}
                          style={[homeStyles.dots, {opacity}]}
                        />
                      );
                    })}
                </View>
              </>
            )}
          </ScrollView>
        </SafeAreaView>
      )}
      {isLoading && <LoadingComponent />}
    </>
  );
};

export default Home;
