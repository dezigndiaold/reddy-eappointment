import axios from 'axios';
import {mainApi} from './constants';

export const login = (doctor_number, password) =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/login`,
      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        doctor_number: doctor_number,
        password: password,
      }),
      responseType: 'json',
    })
      .then(res => {
        console.log(res);
        if (res.status === 200) {
          resolve(res.data);
        } else reject(res);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });

export const signUp = (
  doctor_name,
  doctor_clinic,
  doctor_number,
  password,
  confirm_password,
  morning_from,
  morning_to,
  evening_from,
  evening_to,
) =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/doctor_register`,
      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        doctor_name: doctor_name,
        doctor_clinic: doctor_clinic,
        doctor_number: doctor_number,
        password: password,
        confirm_password: confirm_password,
        morning_from: morning_from,
        morning_to: morning_to,
        evening_from: evening_from,
        evening_to: evening_to,
      }),

      responseType: 'json',
    })
      .then(res => {
        console.log(res);
        if (res.status === 200) {
          resolve(res);
        } else reject(res);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
