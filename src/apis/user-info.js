import axios from 'axios';
import {mainApi} from './constants';

export const getPhoneNumbersList = (doctor_id, phone) =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/appointment_listing`,
      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        patient_number: phone,
        doctor_id: doctor_id,
      }),
      responseType: 'json',
    })
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else reject(res);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });

export const getAppointmentCount = doctor_id =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/getAppoinmentCount`,
      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        doctor_id: doctor_id,
      }),
      responseType: 'json',
    })
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else reject(res);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });

export const getBannerList = () =>
  new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `${mainApi.baseUrl}/api/bannerList`,
      headers: {'Content-Type': 'application/json'},
      responseType: 'json',
    })
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else reject(res);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });

export const getAppointments = doctor_id =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/view_appointment`,

      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        doctor_id: doctor_id,
      }),
      responseType: 'json',
    })
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else reject(res);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });

export const getMorningAvailableTime = (doctor_id, date) =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/docmrng_appointment`,

      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        doctor_id: doctor_id,
        date: date,
      }),
      responseType: 'json',
    })
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else reject(res);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
export const getEveningAvailableTime = (doctor_id, date) =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/docevn_appointment`,

      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        doctor_id: doctor_id,
        date: date,
      }),
      responseType: 'json',
    })
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else reject(res);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
export const editUserDetails = (
  id,
  doctor_name,
  doctor_clinic,
  doctor_number,
  password,
  confirm_password,
  morning_from,
  morning_to,
  evening_from,
  evening_to,
) =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/update_doctor`,
      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        id: id,
        doctor_name: doctor_name,
        doctor_clinic: doctor_clinic,
        doctor_number: doctor_number,
        password: password,
        confirm_password: confirm_password,
        morning_from: morning_from,
        morning_to: morning_to,
        evening_from: evening_from,
        evening_to: evening_to,
      }),

      responseType: 'json',
    })
      .then(res => {
        if (res.status === 200) {
          resolve(res);
        } else reject(res);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });

export const createAppointment = (
  patient_name,
  patient_number,
  doctor_id,
  app_date,
  app_time,
  comments,
) =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/create_appointment`,
      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        patient_name: patient_name,
        patient_number: patient_number,
        doctor_id: doctor_id,
        app_date: app_date,
        app_time: app_time,
        comments: comments,
      }),
      responseType: 'json',
    })
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else reject(res);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });

export const updateAppointment = (
  id,
  doctor_id,
  patient_name,
  patient_number,
  app_date,
  app_time,
  comments,
) =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/update_appointment`,
      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        id: id,
        doctor_id: doctor_id,
        patient_name: patient_name,
        patient_number: patient_number,
        app_date: app_date,
        app_time: app_time,
        comments: comments,
      }),
      responseType: 'json',
    })
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else reject(res);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });

export const closeAppointment = (id, doctor_id, status) =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/close_appointment`,
      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        id: id,
        doctor_id: doctor_id,
        status: status,
      }),
      responseType: 'json',
    })
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else reject(res);
      })
      .catch(error => {
        console.log(id, doctor_id, status);
        console.log(error);
        reject(error);
      });
  });

export const cancelAppointment = (id, doctor_id, status) =>
  new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${mainApi.baseUrl}/api/reject_appointment`,
      headers: {'Content-Type': 'application/json'},
      data: JSON.stringify({
        id: id,
        doctor_id: doctor_id,
        status: status,
      }),
      responseType: 'json',
    })
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else reject(res);
      })
      .catch(error => {
        console.log(error, id, doctor_id, status);
        reject(error);
      });
  });
