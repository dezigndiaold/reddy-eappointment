import {utilsActionTypes} from '../actionTypes';

let initialState = {
  token: null,
  patientDetails: null,
  appointments: [],
};
export const UtilsReducer = (state = initialState, action) => {
  switch (action.type) {
    case utilsActionTypes.SET_TOKEN:
      return {
        ...state,
        token: action.token,
      };
      break;
    case utilsActionTypes.SET_ALL_APPOINTMENTS:
      return {
        ...state,
        appointments: action.appointments,
      };
      break;
    case utilsActionTypes.SET_PATIENT_DETAILS:
      return {
        ...state,
        patientDetails: action.patientDetails,
      };
      break;
  }
  return state;
};
