import {utilsActionTypes} from '../actionTypes';

export const setToken = token => {
  return {
    type: utilsActionTypes.SET_TOKEN,
    token: token,
  };
};
export const setAllAppointments = appointments => {
  return {
    type: utilsActionTypes.SET_ALL_APPOINTMENTS,
    appointments: appointments,
  };
};
export const setPatientDetails = patientDetails => {
  return {
    type: utilsActionTypes.SET_PATIENT_DETAILS,
    patientDetails: patientDetails,
  };
};
