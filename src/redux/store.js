import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {UtilsReducer} from './reducers/utilsReducer';
const rootReducer = combineReducers({
  utils: UtilsReducer,
});
export const store = createStore(rootReducer, applyMiddleware(thunk));
