import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {connect} from 'react-redux';
import {createStackNavigator} from '@react-navigation/stack';
import {setToken} from '../../redux/actions/utilsActions';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from '../screens/splashScreen';
import AuthNavigator from './authNavigator';
import DrawerNavigator from './drawer-navigators/drawerNavigator';
import CreateAppointmentScreen from '../screens/createAppointment/createAppointmentScreen';
import MyProfileScreen from '../screens/myprofile/myProfileScreen';
import SendMessageScreen from '../screens/sendmessage/sendMessageScreen';
import ViewAppointmentScreen from '../screens/viewAppointment/viewAppointmentScreen';
import ViewPatientDetailsScreen from '../screens/viewPatientDetails/viewPatientDetailsScreen';

const Stack = createStackNavigator();

const RootNavigator = props => {
  const [hidesplash, showSplash] = React.useState(false);

  async function fetchToken() {
    try {
      const token = await AsyncStorage.getItem('token');

      if (token) {
        props.setToken(token);
      } else {
        props.setToken(null);
      }
      showSplash(true);
    } catch (error) {
      console.log('fetch token', error);
    }
  }

  React.useEffect(() => {
    setTimeout(() => {
      fetchToken();
    }, 600);
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {!hidesplash ? (
          <Stack.Screen
            name="splash"
            component={SplashScreen}
            options={{
              animationEnabled: false,
              headerShown: false,
            }}
          />
        ) : (
          <>
            {!props.utils.token ? (
              <Stack.Screen
                name="auth"
                component={AuthNavigator}
                options={{
                  animationEnabled: false,
                  headerShown: false,
                }}
              />
            ) : (
              <>
                <Stack.Screen
                  name="drawer"
                  component={DrawerNavigator}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="createappointment"
                  component={CreateAppointmentScreen}
                  options={{
                    headerShown: false,
                  }}
                />
                <Stack.Screen
                  name="myprofile"
                  component={MyProfileScreen}
                  options={{
                    headerShown: false,
                  }}
                />
                <Stack.Screen
                  name="viewappointment"
                  component={ViewAppointmentScreen}
                  options={{
                    headerShown: false,
                  }}
                />
                <Stack.Screen
                  name="sendmessage"
                  component={SendMessageScreen}
                  options={{
                    headerShown: false,
                  }}
                />
                <Stack.Screen
                  name="viewpatientdetails"
                  component={ViewPatientDetailsScreen}
                  options={{
                    headerShown: false,
                  }}
                />
                <Stack.Screen
                  name="auth"
                  component={AuthNavigator}
                  options={{
                    animationEnabled: false,
                    headerShown: false,
                  }}
                />
              </>
            )}
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const mapStateToProps = state => ({
  utils: state.utils,
});
const mapDispatchToProps = dispatch => ({
  setToken: token => dispatch(setToken(token)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RootNavigator);
