import * as React from 'react';
import Animated from 'react-native-reanimated';
import {createDrawerNavigator} from '@react-navigation/drawer';
import CustomDrawer from '../../../components/customDrawer/customDrawer';
import HomeScreen from '../../screens/home/homeScreen';
import {drawerStyles} from '../../../styles/drawerStyles';
const Drawer = createDrawerNavigator();

const DrawerNavigator = props => {
  const [progress, setProgress] = React.useState(new Animated.Value(0));

  return (
    <Drawer.Navigator
      drawerType="slide"
      drawerStyle={[drawerStyles.drawerBody]}
      drawerContent={props => {
        setProgress(props.progress);
        return <CustomDrawer {...props} />;
      }}>
      <Drawer.Screen name="home">
        {props => <HomeScreen {...props} progress={progress} />}
      </Drawer.Screen>
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
