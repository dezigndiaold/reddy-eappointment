import React from 'react';
import SendMessage from '../../../components/sendmessage/sendMessage';

const SendMessageScreen = props => {
  return <SendMessage {...props} />;
};

export default SendMessageScreen;
