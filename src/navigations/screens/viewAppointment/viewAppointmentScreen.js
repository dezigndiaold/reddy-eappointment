import React from 'react';
import ViewAppointment from '../../../components/viewAppointment/viewAppointment';

const ViewAppointmentScreen = props => {
  return <ViewAppointment {...props} />;
};

export default ViewAppointmentScreen;
