import React from 'react';
import AuthSignUp from '../../../components/auth/authSignUp';

const SignUpScreen = props => {
  return <AuthSignUp {...props} />;
};

export default SignUpScreen;
