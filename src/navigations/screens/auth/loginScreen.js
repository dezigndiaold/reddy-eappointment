import React from 'react';
import AuthLogin from '../../../components/auth/authLogin';

const LoginScreen = props => {
  return <AuthLogin {...props} />;
};

export default LoginScreen;
