import React from 'react';
import Animated from 'react-native-reanimated';
import {homeStyles} from '../../../styles/homeStyles';
import Home from '../../../components/home/home';

const HomeScreen = props => {
  const scale = Animated.interpolate(props.progress, {
    inputRange: [0, 1],
    outputRange: [1, 0.8],
  });
  const animatedStyle = {transform: [{scale}]};

  return (
    <Animated.View style={[homeStyles.homeAnimation, animatedStyle]}>
      <Home {...props} />
    </Animated.View>
  );
};

export default HomeScreen;
