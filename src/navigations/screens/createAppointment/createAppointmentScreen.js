import React from 'react';
import CreateAppointment from '../../../components/createAppointment/createAppointment';

const CreateAppointmentScreen = props => {
  return <CreateAppointment {...props} />;
};

export default CreateAppointmentScreen;
