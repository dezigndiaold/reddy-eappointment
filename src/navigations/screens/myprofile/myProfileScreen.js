import React from 'react';
import MyProfile from '../../../components/myprofile/myProfile';

const MyProfileScreen = props => {
  return <MyProfile {...props} />;
};

export default MyProfileScreen;
