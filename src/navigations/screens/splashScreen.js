import React from 'react';
import {View, StatusBar} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {splashStyles} from '../../styles/splashStyles';
import {Thumbnail} from 'native-base';
import logo from '../../assets/images/splash-icon.png';

const SplashScreen = () => {
  return (
    <>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <View style={splashStyles.mainContainer}>
        <Thumbnail square large source={logo} style={splashStyles.icon} />
        <Animatable.Text animation="fadeIn" style={splashStyles.title}>
          eAppointment
        </Animatable.Text>
      </View>
    </>
  );
};

export default SplashScreen;
